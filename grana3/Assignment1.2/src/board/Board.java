package board;

import java.util.*;
import piece.*;
import path.Path;

/**
 * This class represents the chess board, comprising 64 distinct squares.
 * The board controls all piece movements, but each individual piece has to check whether a move is legal.
 */
public class Board {
    private final int rows = 8;
    private final int columns = 8;
    public Square[][] cells = new Square[rows][columns];

    // Coordinates for both kings
    private int[] blackKing = new int[2];
    private int[] whiteKing = new int[2];
    public boolean whiteInCheck = false;
    public boolean blackInCheck = false;

    public Board(int rows, int columns) {
        super();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                this.cells[i][j] = new Square(i, j, null);
            }
        }
    }

    public int getRows() {
        return this.rows;
    }

    public int getColumns() {
        return this.columns;
    }

    public Square[][] getCells() {
        return this.cells;
    }

    /**
     * Instantiate the pawns.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setPawns(int rows, int columns) {
        for (int i = 0; i < columns; i++) {
            this.cells[1][i].occupy(new Pawn(1, i, true, "black"));
            this.cells[rows - 2][i].occupy(new Pawn(rows - 2, i, true, "white"));
        }
    }

    /**
     * Instantiate the rooks.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setRooks(int rows, int columns) {
        this.cells[0][0].occupy(new Rook(0, 0, true, "black"));
        this.cells[0][columns - 1].occupy(new Rook(0, columns - 1, true, "black"));
        this.cells[rows - 1][0].occupy(new Rook(rows - 1, 0, true, "white"));
        this.cells[rows - 1][columns - 1].occupy(new Rook(rows - 1, columns - 1, true, "white"));
    }

    /**
     * Instantiate the knights.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setKnights(int rows, int columns) {
        this.cells[0][1].occupy(new Knight(0, 1, true, "black"));
        this.cells[0][columns - 2].occupy(new Knight(0, columns - 2, true, "black"));
        this.cells[rows - 1][1].occupy(new Knight(rows - 1, 1, true, "white"));
        this.cells[rows - 1][columns - 2].occupy(new Knight(rows - 1, columns - 2, true, "white"));
    }

    /**
     * Instantiate the bishops.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setBishops(int rows, int columns) {
        this.cells[0][2].occupy(new Bishop(0, 2, true, "black"));
        this.cells[0][columns - 3].occupy(new Bishop(0, columns - 3, true, "black"));
        this.cells[rows - 1][2].occupy(new Bishop(rows - 1, 2, true, "white"));
        this.cells[rows - 1][columns - 3].occupy(new Bishop(rows - 1, columns - 3, true, "white"));
    }

    /**
     * Instantiate the queens.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setQueens(int rows, int columns) {
        this.cells[0][3].occupy(new Queen(0, 3, true, "black"));
        this.cells[rows - 1][3].occupy(new Queen(rows - 1, 3, true, "white"));
    }

    /**
     * Instantiate the kings.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setKings(int rows, int columns) {
        this.cells[0][4].occupy(new King(0, 4, true, "black"));
        this.cells[rows - 1][4].occupy(new King(rows - 1, 4, true, "white"));
        this.blackKing[0] = 0;
        this.blackKing[1] = 4;
        this.whiteKing[0] = rows - 1;
        this.whiteKing[1] = 4;
    }

    /**
     * Instantiate the archbishops.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setArchbishops(int rows, int columns) {
        this.cells[0][2].occupy(new Archbishop(0, 2, true, "black"));
        this.cells[0][columns - 3].occupy(new Archbishop(0, columns - 3, true, "black"));
        this.cells[rows - 1][2].occupy(new Archbishop(rows - 1, 2, true, "white"));
        this.cells[rows - 1][columns - 3].occupy(new Archbishop(rows - 1, columns - 3, true, "white"));
    }

    /**
     * Instantiate the Commoners.
     * @param rows Board rows.
     * @param columns Board columns.
     */
    private void setCommoners(int rows, int columns) {
        this.cells[2][2].occupy(new Commoner(2, 2, true, "black"));
        this.cells[2][columns - 3].occupy(new Commoner(2, columns - 3, true, "black"));
        this.cells[rows - 3][2].occupy(new Commoner(rows - 3, 2, true, "white"));
        this.cells[rows - 3][columns - 3].occupy(new Commoner(rows - 3, columns - 3, true, "white"));
        this.cells[2][0].occupy(new Commoner(2, 0, true, "black"));
        this.cells[2][columns - 1].occupy(new Commoner(2, columns - 1, true, "black"));
        this.cells[rows - 3][0].occupy(new Commoner(rows - 3, 0, true, "white"));
        this.cells[rows - 3][columns - 1].occupy(new Commoner(rows - 3, columns - 1, true, "white"));
    }

    /**
     * This method takes care of placing all the pieces for the beginning of a game
     */
    public void init() {
        this.setRooks(this.rows, this.columns);
        this.setKnights(this.rows, this.columns);
        //this.setBishops(this.rows, this.columns);
        this.setArchbishops(this.rows, this.columns);
        this.setQueens(this.rows, this.columns);
        this.setKings(this.rows, this.columns);
        this.setPawns(this.rows, this.columns);
        this.setCommoners(this.rows, this.columns);
    }

    /**
     * Occupies a given square.
     * @param piece The moving piece.
     * @param destination The square to be occupied.
     * @return nothing.
     */
    private void occupyCell(Piece piece, Square destination) {
        destination.occupy(piece);
        piece.setRowCoord(destination.getRowCoord());
        piece.setColCoord(destination.getColCoord());

        if (piece instanceof King) {
            switch (piece.getColor()) {
                case "white":
                    this.whiteKing[0] = destination.getRowCoord();
                    this.whiteKing[1] = destination.getColCoord();
                    break;
                case "black":
                    this.blackKing[0] = destination.getRowCoord();
                    this.blackKing[1] = destination.getColCoord();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Checks whether a piece can move to a given destination.
     * @param initial The selected square.
     * @param destination  The square to which the selected piece is to move.
     * @return boolean Whether the piece can move.
     */
    public boolean movePiece(Square initial, Square destination) {
        Piece movingPiece = initial.getPiece();

        int rowCoord = destination.getRowCoord();
        int colCoord = destination.getColCoord();

        if (!movingPiece.movesWithinBoard(this, rowCoord, colCoord)) {
            return false;
        } else {
            Path path = movingPiece.makePath(this, rowCoord, colCoord);

            if (!isValidPath(path, (movingPiece instanceof Knight || movingPiece instanceof Archbishop))) {
                return false;
            }
            if (!destination.isAvailable()) {
                Piece currPiece = destination.getPiece();
                if (currPiece.getColor().equals(movingPiece.getColor())) {
                    return false;
                }
            }

            this.occupyCell(movingPiece, destination);
            initial.vacate();
            if (movingPieceCanCheckWhite(movingPiece)) {
                System.out.println("White is in check");
                this.whiteInCheck = true;
            }
            if (movingPieceCanCheckBlack(movingPiece)) {
                System.out.println("Black is in check");
                this.blackInCheck = true;
            }

            if (movingPiece instanceof King) {
                switch(movingPiece.getColor()) {
                    case "white":
                        if (this.whiteInCheck) {
                            this.whiteInCheck = false;
                        }
                        break;
                    case "black":
                        if (this.blackInCheck) {
                            this.blackInCheck = false;
                        }
                        break;
                }
            }

            return true;
        }
    }

    /**
     * Checks whether a given path has any piece in the way.
     * @param path The moving piece.
     * @param canHop Whether the piece can hop over other pieces.
     * @return boolean Whether the given path is valid.
     */
    private boolean isValidPath(Path path, boolean canHop) {
        List<int[]> cellsPath = path.getCoordinates();

        if (!path.isValid()) {
            return false;
        } else {
            if (canHop) {
                return true;
            }
        }

        // This is a flag to skip the first entry of each path, since it will always be occupied by the moving piece
        boolean firstEntry = true;
        for (int[] coords : cellsPath) {
            if (firstEntry) {
                firstEntry = false;
                continue;
            }

            if (!cells[coords[0]][coords[1]].isAvailable()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether a moving piece will cause the white king to be in check
     * @param piece The moving piece.
     * @return boolean Whether the given piece can cause the white king to be in check.
     */
    private boolean movingPieceCanCheckWhite(Piece piece) {
        int kingRowCoord = this.whiteKing[0];
        int kingColCoord = this.whiteKing[1];

        if (piece.getColor().equals("white")) {
            return false;
        }

        Path pathToKing = piece.makePath(this, kingRowCoord, kingColCoord);
        if (isValidPath(pathToKing, (piece instanceof Knight || piece instanceof Archbishop))) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether a moving piece will cause the black king to be in check
     * @param piece The moving piece.
     * @return boolean Whether the given piece can cause the black king to be in check.
     */
    private boolean movingPieceCanCheckBlack(Piece piece) {
        int kingRowCoord = this.blackKing[0];
        int kingColCoord = this.blackKing[1];

        if (piece.getColor().equals("black")) {
            return false;
        }

        Path pathToKing = piece.makePath(this, kingRowCoord, kingColCoord);
        if (isValidPath(pathToKing, (piece instanceof Knight || piece instanceof Archbishop))) {
            return true;
        }

        return false;
    }

    /**
     * Checks whether the white king is in check.
     * @return Whether king is in check.
     */
    public boolean whiteInCheck() {
        return this.whiteInCheck;
    }

    /**
     * Checks whether the black king is in checkmate.
     * @return Whether king is in check.
     */
    public boolean blackInCheck() {
        return this.blackInCheck;
    }

    /**
     * Checks whether the white king is in checkmate.
     * @return Whether king is in checkmate.
     */
    public boolean whiteInCheckMate() {
        int kingRowCoord = this.whiteKing[0];
        int kingColCoord = this.whiteKing[1];

        if (!this.cells[kingRowCoord][kingColCoord].getPiece().hasValidMoves(this)) {
            return true;
        }

        return false;
    }

    /**
     * Checks whether the black king is in checkmate.
     * @return Whether king is in checkmate.
     */
    public boolean blackInCheckMate() {
        int kingRowCoord = this.blackKing[0];
        int kingColCoord = this.blackKing[1];

        if (!this.cells[kingRowCoord][kingColCoord].getPiece().hasValidMoves(this)) {
            return true;
        }

        return false;
    }

    /**
     * Moves piece at destination square to initial square.
     * @param destination The square to which a piece just moved.
     * @param initial The square from which a piece just moved.
     */
    public void undoMove(Square destination, Square initial) {
        Piece movingPiece = destination.getPiece();
        this.occupyCell(movingPiece, initial);
        destination.vacate();
    }
}
