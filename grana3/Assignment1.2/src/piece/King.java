package piece;

import board.Square;
import path.Path;
import board.Board;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.max;

public class King extends Piece {
    public King(int x, int y, boolean playable, String color) {
        super(x, y, playable, color);
    }

    /**
     * This method creates a path object with the coordinates between a piece and a given location
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Path from the piece's current coordinates to those given.
     */
    @Override
    public Path makePath(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        // Instantiate a path.Path object
        Path path = new Path();
        if (!isLegalMove(board, newRowCoord, newColCoord)) {
            return path;
        }

        path.addCoords(rowCoord, colCoord);
        path.makeValid();

        return path;
    }

    /**
     * This method checks whether a move to the given coordinates is legal.
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return boolean Whether the move is legal for the current piece.
     */
    @Override
    public boolean isLegalMove(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        if (max(Math.abs(newRowCoord - rowCoord), Math.abs(newColCoord - colCoord)) > 1) {
            return false;
        }

        return true;
    }

    /**
     * If the king is in check, determine whether it has any valid moves.
     * @param board The board in which the piece is to move.
     * @return boolean Whether the king has any valid moves.
     */
    @Override
    public boolean hasValidMoves(Board board) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        Square[][] cells = board.getCells();

        // A king can only move to one out of a maximum of 8 spots at any moment
        final int numSpots = 8;

        // This list contains all 8 available spots to which a king can move relative to its position
        List<int[]> validSpots = new ArrayList<int[]>();
        addValidSpots(validSpots, rowCoord, colCoord);

        for (int i = 0; i < numSpots; i++) {
            int[] currCoords = validSpots.get(i);

            // First check whether the spot is within the board, then check whether it's available
            if (super.movesWithinBoard(board, currCoords[0], currCoords[1])) {
                if (cells[currCoords[0]][currCoords[1]].isAvailable()) {
                    return true;
                }
            }
        }

        return false;
    }

    // Helper for considering all possible moves
    private void addValidSpots(List<int[]> validSpots, int rowCoord, int colCoord) {
        validSpots.add(new int[]{rowCoord - 1, colCoord});
        validSpots.add(new int[]{rowCoord, colCoord - 1});
        validSpots.add(new int[]{rowCoord - 1, colCoord - 1});
        validSpots.add(new int[]{rowCoord + 1, colCoord});
        validSpots.add(new int[]{rowCoord, colCoord + 1});
        validSpots.add(new int[]{rowCoord + 1, colCoord + 1});
        validSpots.add(new int[]{rowCoord - 1, colCoord + 1});
        validSpots.add(new int[]{rowCoord + 1, colCoord - 1});
    }
}