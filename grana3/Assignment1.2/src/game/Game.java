package game;

/**
 * This is where the game loop and GUI are to be implemented.
 * The game class will instantiate 2 players and a GUI.
 */
public class Game {
    // White player always goes first
    public static Turn turn = Turn.WHITE;

    public static void main(String args[]) {
        Gui gui = new Gui("Chessboard", turn);
    }
}