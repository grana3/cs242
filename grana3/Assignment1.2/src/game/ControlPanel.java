package game;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class contains the new game and quit buttons.
 */
public class ControlPanel extends JPanel {
    private ChessBoard board;

    public ControlPanel(ChessBoard board) {
        this.board = board;
        JButton quit = new JButton("Forfeit");
        JButton newGame = new JButton("New Game");
        JButton undo = new JButton("Undo");
        add(quit);
        add(newGame);
        add(undo);

        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(null, "Start new game?");
                if(dialogResult == JOptionPane.YES_OPTION){
                    board.newGame();
                }
            }
        });

        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"White", "Black"};
                int dialogResult = JOptionPane.showOptionDialog(null, "Which player is forfeiting?", "Forfeit",
                        JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options,null);
                if (dialogResult == 0) {
                    board.labels.blackWins();
                    board.newGame();
                } else {
                    board.labels.whiteWins();
                    board.newGame();
                }
            }
        });

        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                board.undoMove();
            }
        });

        this.setVisible(true);
    }
}
