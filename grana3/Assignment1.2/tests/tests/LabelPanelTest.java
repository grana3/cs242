package tests;

import game.LabelPanel;
import game.Turn;
import org.junit.Test;

import static org.junit.Assert.*;

public class LabelPanelTest {

    @Test
    public void changeTurn() {
        Turn turn = Turn.WHITE;
        LabelPanel labels = new LabelPanel(turn);

        labels.changeTurn();
        assertEquals(labels.turn, Turn.BLACK);
    }

    @Test
    public void whiteWins() {
        Turn turn = Turn.WHITE;
        LabelPanel labels = new LabelPanel(turn);

        labels.whiteWins();
        assertEquals(1, labels.getWhiteScore());
    }

    @Test
    public void blackWins() {
        Turn turn = Turn.WHITE;
        LabelPanel labels = new LabelPanel(turn);

        labels.blackWins();
        assertEquals(1, labels.getBlackScore());
    }
}