process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./server.js');
let should = chai.should();

chai.use(chaiHttp);
describe('Actors', () => {
    describe('/GET actor', () => {
        it('gets all actors', (done) => {
            chai.request(server)
                .get('/actors')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

});