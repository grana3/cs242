const Node = require("./node.js");

/**
 * The movie class extends the node class to support movies.
 */
class Movie extends Node {
    constructor(name, year, boxOffice, actors) {
        super();
        this.nodeType = "Movie";
        this.name = name;
        this.year = year;
        this.boxOffice = boxOffice;
        this.actors = actors;
    }

    getYear() {
        return this.year;
    }

    getBoxOffice() {
        return this.boxOffice;
    }

    getActors() {
        return this.actors;
    }
}

module.exports = Movie;