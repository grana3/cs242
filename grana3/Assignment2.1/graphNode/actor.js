const Node = require("./node.js");

/**
 * The actor class extends the node class to support actors.
 */
class Actor extends Node {
    constructor(name, age, totalGross, movies) {
        super();
        this.nodeType = "Actor";
        this.name = name;
        this.age = age;
        this.totalGross = totalGross;
        this.movies = movies;
        this.connections = 0;
    }

    getAge() {
        return this.age;
    }

    getTotalGross() {
        return this.totalGross;
    }

    getMovies() {
        return this.movies;
    }

    getConnections() {
        return this.connections;
    }

    getAge() {
        return this.age;
    }

    addConnections(connections) {
        this.connections += connections;
    }
}


module.exports = Actor;