import React from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { TabNavigator, SafeAreaView  } from 'react-navigation';

import styles from '../styles.js';

class Error extends React.Component {
  render() {
    return(
      <SafeAreaView>
        <View style={styles.error}>
          <Image
            style={{width: 75, height: 75}}
            source={require('../images/error_icon.png')}
          />
          <Text>{"\n"}There seems to have been an error</Text>
        </View>
      </SafeAreaView>
    );
  }
}

export default Error;
