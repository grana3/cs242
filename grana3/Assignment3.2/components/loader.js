import React from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { TabNavigator, SafeAreaView  } from 'react-navigation';

import styles from '../styles.js';

class Loader extends React.Component {
  render() {
    return(
      <SafeAreaView>
        <View style={styles.loader}>
          <Image
            style={{width: 75, height: 75}}
            source={require('../images/logo.png')}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default Loader;
