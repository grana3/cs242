import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';

import styles from '../styles.js';

class NotificationCard extends React.Component {
  render() {
    return (
      <View style={styles.repocard}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            source={require('../images/notifications_active.png')}
            style={{width: 21, height: 21}}
          />
          <Text
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
            onPress={() => Linking.openURL(this.props.notif.url)}>
            {this.props.notif.repository.full_name}
          </Text>
        </View>
        <Text>{this.props.notif.subject.title}</Text>
      </View>
    );
  }
}

export default NotificationCard;
