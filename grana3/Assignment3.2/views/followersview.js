import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView, TextInput, TouchableHighlight } from 'react-native';
import { StackNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import Error from '../components/error.js';
import FollowerCard from '../components/followercard.js';
import ExternalProfileView from './externalprofileview.js';
import SearchResults from './searchresults.js';

class Followers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      renderSearch: false,
      searchText: ""
    };
  }

  /**
   * Helper function that displays the search bar when icon is clicked.
   */
  displaySearch = () => {
    if (this.state.renderSearch) {
      return (
        <TextInput
          style={{borderColor: '#d5d5d5', borderWidth: 1, borderRadius: 8, padding: 15, marginTop: 20, width: '100%'}}
          onChangeText={(text) => this.setState({searchText: text})}
          onSubmitEditing={this.search}
          placeholder={"Search"}
        />
      );
    }

    return null;
  }

  search = () => {
    this.props.navigation.navigate('Results', {query: this.state.searchText, type: 'users'});
  }


  render() {
    /**
     * Check for network error.
     */
    if (this.props.screenProps.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.props.screenProps.followers === null) {
      return (
        <Loader></Loader>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        {this.displaySearch()}
        <View style={{flex: 1, flexDirection: 'row', paddingTop: 20}}>
          <Text style={{fontSize: 28, marginBottom: 20}}>Followers &nbsp;
            <Text style={{fontSize: 14, paddingBottom: 5}}>
              {this.props.screenProps.followers.length}
            </Text>
          </Text>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 10}}>
            <TouchableHighlight
              onPress={() => {
                let renderFlag = !this.state.renderSearch;
                this.setState(previousState => {
                  return {
                    renderSearch: renderFlag
                  };
                }
              )}}>
              <Image
                source={require('../images/search_icon.png')}
                style={{width: 20, height: 20}}
              />
            </TouchableHighlight>
          </View>
        </View>
        {this.props.screenProps.followers.map((item, i) => {
          return (
            <FollowerCard key={i} follower={item} navigation={this.props.navigation} />
          );
        })}
      </ScrollView>
    );
  }
}

const RootStack = StackNavigator(
  {
    Followers: {
      screen: Followers,
    },
    Profile: {
      screen: ExternalProfileView,
    },
    Results: {
      screen: SearchResults,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Followers',
  }
);

class FollowersView extends React.Component {
  render() {
    return <RootStack screenProps={this.props.screenProps}/>;
  }
}

export default FollowersView;
