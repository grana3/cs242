import React from 'react';
import { Button, StyleSheet, Text, View, Image, TextInput, Linking, ScrollView, TouchableHighlight } from 'react-native';
import { StackNavigator, TabNavigator, SafeAreaView } from 'react-navigation';
import { SearchBar } from 'react-native-elements';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import Error from '../components/error.js';
import RepoCard from '../components/repocard.js';
import SearchResults from './searchresults.js';

class Repos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      renderSearch: false,
      searchText: ""
    };
  }

  /**
   * Custom comparator for repos to determine which has more stars.
   */
  compareRepos = (first, second) => {
    if (first.stargazers_count < second.stargazers_count) {
      return -1;
    } else if (first.stargazers_count > second.stargazers_count) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * Helper function that displays the search bar when icon is clicked.
   */
  displaySearch = () => {
    if (this.state.renderSearch) {
      return (
        <TextInput
          style={{borderColor: '#d5d5d5', borderWidth: 1, borderRadius: 8, padding: 15, marginTop: 20, width: '100%'}}
          onChangeText={(text) => this.setState({searchText: text})}
          onSubmitEditing={this.search}
          placeholder={"Search"}
        />
      );
    }

    return null;
  }

  search = () => {
    this.props.navigation.navigate('Results', {query: this.state.searchText, type: 'repos'});
  }

  render() {
    /**
     * Check for network error.
     */
    if (this.props.screenProps.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.props.screenProps.repos === null) {
      return (
        <Loader></Loader>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        {this.displaySearch()}
        <View style={{flex: 1, flexDirection: 'row', paddingTop: 20}}>
          <Text style={{fontSize: 28, marginBottom: 20}}>Repos &nbsp;
            <Text style={{fontSize: 14, paddingBottom: 5}}>
              {this.props.screenProps.repos.length}
            </Text>
          </Text>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 10}}>
            <TouchableHighlight
              onPress={() => {
                let renderFlag = !this.state.renderSearch;
                this.setState(previousState => {
                  return {
                    renderSearch: renderFlag
                  };
                }
              )}}>
              <Image
                source={require('../images/search_icon.png')}
                style={{width: 20, height: 20}}
              />
            </TouchableHighlight>
          </View>
        </View>
        {this.props.screenProps.repos.sort(this.compareRepos).map((item, i) => {
          if (item.description === null) {
            return (
              <RepoCard key={i} repo={item} noDescription />
            );
          }
          return (
            <RepoCard key={i} repo={item} />
          );
        })}
      </ScrollView>
    );
  }
}

const RootStack = StackNavigator(
  {
    Repos: {
      screen: Repos,
    },
    Results: {
      screen: SearchResults,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Repos',
  }
);

class ReposView extends React.Component {
  render() {
    return <RootStack screenProps={this.props.screenProps}/>;
  }
}

export default ReposView;
