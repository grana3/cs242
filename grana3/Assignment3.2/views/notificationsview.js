import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView, TouchableHighlight } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import ProfileCard from '../components/profilecard.js';
import NotificationCard from '../components/notificationcard.js';
import Error from '../components/error.js';

class NotificationsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: []
    }
  }

  componentDidMount() {
    const token = '833010fe365d49c75386040c6141671474f740a2';
    let notificationData = null;

    axios.get('https://api.github.com/notifications', {
      headers: { 'Authorization': 'token ' + token }
    }).then((response) => {
      console.log(response.data);
      notificationData = response.data;
    })
    .catch((error) => {
      console.log(error);
    });

    /**
     * Wait for data to return, then set state accordingly.
     */
    setTimeout(() => {
      this.setState(previousState => {
        return {
          notifications: notificationData
        };
      });
    }, 500);
  }

  render() {
    /**
     * Check whether axios has returned data.
     */
    if (this.state.notifications === null) {
      return (
        <Loader></Loader>
      );
    };

    noNotifs = () => {
      if (this.state.notifications.length === 0) {
        return <Text>No notifications to show.</Text>
      }
    }

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        <View style={{flex: 1, flexDirection: 'row', paddingTop: 20}}>
          <Text style={{fontSize: 28, marginBottom: 20}}>Notifications &nbsp;
            <Text style={{fontSize: 14, paddingBottom: 5}}>
              {this.state.notifications.length}
            </Text>
          </Text>
        </View>
        {noNotifs()}
        {this.state.notifications.map((item, i) => (
          <NotificationCard key={i} notif={item} />
        ))}
      </ScrollView>
    );
  }
}

export default NotificationsView;
