import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import ProfileView from './views/profileview.js';
import ReposView from './views/reposview.js';
import FollowersView from './views/followersview.js';
import FollowingView from './views/followingview.js';
import Login from './login.js';
import Router from './router.js';
import styles from './styles.js';

// https://inducesmile.com/facebook-react-native/react-native-layout-example-tutorial/
// https://github.com/GeekyAnts/react-native-easy-grid/

/**
 * This is the root component where the log in logic is implemented.
 */
const RootStack = TabNavigator(
  {
    SignedIn: {
      screen: Router
    },
    SignedOut: {
      screen: Login
    }
  },
  {
    headerMode: "none",
    mode: "modal",
    initialRouteName: "SignedOut",
    tabBarOptions: {
      style: {
        display: 'none',
      },
      labelStyle: {
        display: 'none',
      }
    },
  }
);

class App extends React.Component {
  render() {
    return (
      <RootStack
        screenProps={{
          navigation: this.props.navigation
        }}/>
    );
  }
}

export default App;
