import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  login: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: '8%',
    paddingRight: '8%',
    paddingTop: '50%',
    alignItems: 'center'
  },
  profileView: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: '8%',
    paddingRight: '8%',
    backgroundColor: '#ffffff'
  },
  reposView: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: '8%',
    paddingRight: '8%',
    backgroundColor: '#ffffff'
  },
  followers: {

  },
  following: {

  },
  profilecard: {
    shadowOffset: {width: 0, height: 0},
    shadowColor: 'black',
    shadowOpacity: 0.15,
    shadowRadius: 1,
    borderRadius: 6,
    padding: 10,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'column',
    flexShrink: 0,
    height: 100
  },
  repocard: {
    shadowOffset: {width: 0, height: 0},
    shadowColor: 'black',
    shadowOpacity: 0.15,
    shadowRadius: 1,
    borderRadius: 6,
    padding: 10,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'column',
    flexShrink: 0,
    height: 100
  },
  loader: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#ffffff'
  },
  error: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  link: {
    color: '#4a90e2'
  },
  badge: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  input: {
    borderColor: '#d5d5d5',
    borderWidth: 1,
    borderRadius: 8,
    padding: 15,
    marginTop: 20,
    width: '80%'
  }
});

export default styles;
