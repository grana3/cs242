import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import Error from '../components/error.js';
import RepoCard from '../components/repocard.js'

class ReposView extends React.Component {
  /**
   * Custom comparator for repos to determine which has more stars.
   */
  compareRepos = (first, second) => {
    if (first.stargazers_count < second.stargazers_count) {
      return -1;
    } else if (first.stargazers_count > second.stargazers_count) {
      return 1;
    } else {
      return 0;
    }
  }

  render() {
    /**
     * Check for network error.
     */
    if (this.props.screenProps.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.props.screenProps.repos === null) {
      return (
        <Loader></Loader>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        <Text style={{fontSize: 28, marginBottom: 20, paddingTop: 20}}>Repos &nbsp;
          <Text style={{fontSize: 14, paddingBottom: 5}}>
            {this.props.screenProps.repos.length}
          </Text>
        </Text>
        {this.props.screenProps.repos.sort(this.compareRepos).map((item, i) => {
          if (item.description === null) {
            return (
              <RepoCard key={i} repo={item} noDescription />
            );
          }
          return (
            <RepoCard key={i} repo={item} />
          );
        })}
      </ScrollView>
    );
  }
}

export default ReposView;
