package tests;

import board.Board;
import path.Path;
import piece.Archbishop;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArchbishopTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        Archbishop archbishop = new Archbishop(0, 2, true, "black");
        int newXCoord = 4;
        int newYCoord = 6;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = archbishop.makePath(board, newXCoord, newYCoord);
        Path wrongPath = archbishop.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}