package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.Knight;

import static org.junit.jupiter.api.Assertions.*;

class KnightTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        Knight knight = new Knight(0, 1, true, "black");
        int newXCoord = 2;
        int newYCoord = 2;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = knight.makePath(board, newXCoord, newYCoord);
        Path wrongPath = knight.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}