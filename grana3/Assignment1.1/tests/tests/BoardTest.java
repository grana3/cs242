package tests;

import board.Board;
import board.Square;
import org.junit.jupiter.api.Test;
import piece.*;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void getRows() {
        Board board = new Board(8, 8);

        assertEquals(8, board.getRows());
    }

    @Test
    void getColumns() {
        Board board = new Board(8, 8);

        assertEquals(8, board.getColumns());
    }

    @Test
    void getCells() {
        Board board = new Board(8, 8);

        assertNotNull(board.getCells());
    }

    @Test
    void init() {
        Board board = new Board(8, 8);
        Square[][] cells = board.getCells();

        int numPawns = 0;
        int numRooks = 0;
        int numKnights = 0;
        int numBishops = 0;
        int numQueens = 0;
        int numKings = 0;

        board.init();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece piece = cells[i][j].getPiece();
                System.out.println("Checking piece");
                if(piece instanceof Pawn) {
                    numPawns++;
                    System.out.println("Pawn");
                }
                if(piece instanceof Rook) {
                    numRooks++;
                    System.out.println("Rook");
                }
                if(piece instanceof Knight) {
                    numKnights++;
                    System.out.println("Knight");
                }
                if(piece instanceof Bishop) {
                    numBishops++;
                    System.out.println("Bishop");
                }
                if(piece instanceof Queen) {
                    numQueens++;
                    System.out.println("Queen");
                }
                if(piece instanceof King) {
                    numKings++;
                    System.out.println("King");
                }
            }
        }

        assertEquals(16, numPawns);
        assertEquals(4, numRooks);
        assertEquals(4, numKnights);
        assertEquals(4, numBishops);
        assertEquals(2, numQueens);
        assertEquals(2, numKings);
    }

    @Test
    void movePiece() {
        Board board = new Board(8, 8);
        Square[][] cells = board.getCells();
        Pawn pawn = new Pawn(0, 0, true, "white");
        cells[0][0].occupy(pawn);

        board.movePiece(cells[0][0], cells[0][1]);

        assertNotNull(cells[0][0].getPiece());
        assertNull(cells[0][1].getPiece());
    }
}