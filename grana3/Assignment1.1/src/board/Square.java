package board;

import piece.Piece;

/**
 * This is the class representing each of 64 squares on the chess board.
 * A square only keeps track of its coordinates and the piece it holds.
 */
public class Square {
    public final int rowCoord;
    public final int colCoord;
    public Piece piece;

    public Square(int x, int y, Piece newPiece) {
        super();
        this.rowCoord = x;
        this.colCoord = y;
        this.piece = newPiece;
    }

    public int getRowCoord() {
        return this.rowCoord;
    }

    public int getColCoord() {
        return this.colCoord;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public boolean isAvailable() {
        // Check whether this square has a piece
        return (this.getPiece() == null || !this.getPiece().isInPlay());
    }

    /**
     * Occupies the square with a given piece.
     * @param movingPiece The given piece.
     * @return nothing.
     */
    public void occupy(Piece movingPiece) {
        // If the square is available, simply set its piece to be the moving piece
        if (this.isAvailable()) {
            this.piece = movingPiece;
        } else {
            if (movingPiece.getColor() == this.piece.getColor()) {
                return;
            } else {
                this.piece.deletePiece();
                this.piece = movingPiece;
                movingPiece.setRowCoord(this.getRowCoord());
                movingPiece.setColCoord(this.getColCoord());
            }
        }
    }

    public void vacate() {
        if (this.piece == null) {
            return;
        }
        this.piece = null;
    }
}