package game;

import java.awt.*;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

/**
 * This class acts as a JFrame container for the board and control panel.
 * The purpose of this container is to separate the controls from the actual play area.
 */
public class Gui extends JFrame {
    private final int rows = 8;
    private final int columns = 8;
    public static ChessBoard board;

    public Gui(String title) {
        ControlPanel controls = new ControlPanel();
        this.board = new ChessBoard(rows, columns);
        this.board.init();

        add(controls, BorderLayout.PAGE_START);
        add(this.board, BorderLayout.CENTER);

        this.setTitle(title);
        this.setSize(650, 675);
        this.setVisible(true);
    }
}