package game;

/**
 * This is where the game loop and GUI are to be implemented.
 * The game class will instantiate 2 players and a GUI.
 */
public class Game {
    private final Player whitePlayer = new Player("white", true);
    private final Player blackPlayer = new Player("black", true);

    // White player always goes first
    private Turn turn = Turn.WHITE;

    public static void main(String args[]) {
        Gui gui = new Gui("Chessboard");
    }
}