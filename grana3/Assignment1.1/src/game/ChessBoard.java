package game;

import board.Board;
import board.Square;
import piece.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class acts as the graphical representation of our chess board.
 * Piece images and colors taken from https://en.wikipedia.org/wiki/Chess_piece.
 */
public class ChessBoard extends JPanel {
    private final int rows;
    private final int columns;
    private final String PATH = "/Users/cristobalgrana/Documents/cs242/grana3/Assignment1.1/src/media/";
    public JButton[][] spots;
    public Board logicBoard;

    public ChessBoard(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.spots = new JButton[rows][columns];

        Color black = new Color(200, 141, 83);
        Color white = new Color(248, 207, 164);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j % 2 == 0) {
                    JButton button = new JButton();
                    paintButton(white, button);
                    add(button);
                    this.spots[i][j] = button;
                } else {
                    JButton button = new JButton();
                    paintButton(black, button);
                    add(button);
                    this.spots[i][j] = button;
                }

                if (j == 7) {
                    Color swapper = white;
                    white = black;
                    black = swapper;
                }
            }
        }

        this.setLayout(new GridLayout(8, 8));
        this.setSize(600, 600);
        this.setVisible(true);
    }

    /**
     * Helper function to set a button's color and visibility.
     * @param color The color to paint the button.
     * @param button The button to paint.
     */
    private void paintButton(Color color, JButton button) {
        button.setOpaque(true);
        button.setBackground(color);
        button.setBorderPainted(false);
    }

    /**
     * Analog to method init() in the Board class.
     * Also used to refresh the board after a piece has moved.
     */
    public void init() {
        this.logicBoard = new Board(this.rows, this.columns);
        this.logicBoard.init();

        refreshBoard();

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                int rowCoord = i;
                int colCoord = j;
                this.spots[i][j].addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        JOptionPane.showMessageDialog(null, "(" + rowCoord + ", " + colCoord + ")");
                    }
                });
            }
        }
    }

    private void refreshBoard() {
        Square[][] cells = this.logicBoard.getCells();

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                if (cells[i][j].getPiece() instanceof Pawn) {
                    setImage(cells, i, j, "white_pawn.png", "black_pawn.png");

                } else if (cells[i][j].getPiece() instanceof Rook) {
                    setImage(cells, i, j, "white_rook.png", "black_rook.png");

                } else if (cells[i][j].getPiece() instanceof Knight) {
                    setImage(cells, i, j, "white_knight.png", "black_knight.png");

                } else if (cells[i][j].getPiece() instanceof Bishop) {
                    setImage(cells, i, j, "white_bishop.png", "black_bishop.png");

                } else if (cells[i][j].getPiece() instanceof Queen) {
                    setImage(cells, i, j, "white_queen.png", "black_queen.png");

                } else if (cells[i][j].getPiece() instanceof King) {
                    setImage(cells, i, j, "white_king.png", "black_king.png");

                } else {
                    System.out.println("No piece: " + (cells[i][j].getPiece() == null));
                }
            }
        }
    }

    /**
     * Helper function to set a button's background image.
     * @param cells The array of squares representing the board.
     * @param i The row coordinate of the square.
     * @param j The column coordinate of the square.
     * @param whitePiece The white image of the corresponding piece.
     * @param blackPiece The black image of the corresponding piece.
     */
    private void setImage(Square[][] cells, int i, int j, String whitePiece, String blackPiece) {
        ImageIcon img = null;
        if (cells[i][j].getPiece().getColor() == "white") {
            img = new ImageIcon(PATH + whitePiece);
        } else {
            img = new ImageIcon(PATH + blackPiece);
        }
        this.spots[i][j].setIcon(img);
    }

    public void movePiece(int rowCoord, int colCoord, int newRowCoord, int newColCoord) {
        Square[][] cells = this.logicBoard.getCells();
        boolean moveSuccess = this.logicBoard.movePiece(cells[rowCoord][colCoord], cells[newRowCoord][newColCoord]);

        if (!moveSuccess) {
            System.out.println("Invalid move");
            return;
        }

        this.refreshBoard();
        this.spots[rowCoord][colCoord].setIcon(null);
    }
}
