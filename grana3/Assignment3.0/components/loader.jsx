import React from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { TabNavigator } from 'react-navigation';

import styles from './styles.jsx';

class Loader extends React.Component {
  render() {
    return(
      <Text style={styles.loader}>Repos View</Text>
    );
  }
}

export default Loader;
