import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

// import ProfileView from '/views/profileview.jsx';
// import ReposView from '/views/reposview.jsx';
// import FollowersView from '/views/follwersview.jsx';
// import FollowingView from '/views/follwingview.jsx';
// import styles from '/styles.jsx';
//
// https://inducesmile.com/facebook-react-native/react-native-layout-example-tutorial/
// https://github.com/GeekyAnts/react-native-easy-grid/blob/master/Components/Grid.js

class Loader extends React.Component {
  render() {
    return(
      <SafeAreaView>
        <View style={styles.loader}>
          <Image
            style={{width: 75, height: 75}}
            source={require('./images/logo.png')}
          />
        </View>
      </SafeAreaView>
    );
  }
}

class ProfileView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      repos: [],
      followers: [],
      following: []
    };
  }
  /**
   * Before mounting component, fetch user, repo, and follwer data.
   */
  componentDidMount() {
    const url = 'https://api.github.com/users/cristobalwee';
    let userData = null;
    let repoData = null;
    let followerData = null;
    let followingData = null;
    axios.get(url).then(function (response) {
      userData = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('https://api.github.com/users/cristobalwee/repos').then(function (response) {
      repoData = response.data
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('https://api.github.com/users/cristobalwee/followers').then(function (response) {
      followerData = response.data
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('https://api.github.com/users/cristobalwee/following').then(function (response) {
      followingData = response.data
    })
    .catch(function (error) {
      console.log(error);
    });
    setTimeout(() => {
      this.setState(previousState => {
        return {
          user: userData,
          repos: repoData,
          followers: followerData,
          following: followingData
        };
      });
    }, 500);
  }

  /**
   * Custom comparator for repos to determine which has more stars.
   */
  compareRepos = (first, second) => {
    if (first.stargazers_count < second.stargazers_count) {
      return -1;
    } else if (first.stargazers_count > second.stargazers_count) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * Profile card component.
   */
  renderProfileCard(type) {
    if (type === 'repos') {
      return (
        <View style={styles.profilecard}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Image
              source={require('./images/repos_active.png')}
              style={{width: 20, height: 22}}
            />
            <Text
              style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
              onPress={() => this.props.navigation.navigate('Repos')}>
              Repos
            </Text>
            <View style={styles.badge}>
              <Text>{this.state.user.public_repos}</Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.repos[0].full_name}</Text>
            <View style={styles.badge}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>
                {this.state.repos[0].stargazers_count}
                <Image
                  source={require('./images/star_icon.png')}
                  style={{width: 17, height: 17, marginLeft: 5}}
                />
              </Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.repos[1].full_name}</Text>
            <View style={styles.badge}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>
                {this.state.repos[1].stargazers_count}
                <Image
                  source={require('./images/star_icon.png')}
                  style={{width: 17, height: 17, marginLeft: 5}}
                />
              </Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.repos[2].full_name}</Text>
            <View style={styles.badge}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>
                {this.state.repos[2].stargazers_count}
                <Image
                  source={require('./images/star_icon.png')}
                  style={{width: 17, height: 17, marginLeft: 5}}
                />
              </Text>
            </View>
          </View>
        </View>
      );
    } else if (type === 'followers') {
      return (
        <View style={styles.profilecard} onPress={() => this.props.navigation.navigate('Followers')}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Image
              source={require('./images/followers_active.png')}
              style={{width: 22, height: 21}}
            />
            <Text
              onPress={() => this.props.navigation.navigate('Repos')}
              style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}>Followers</Text>
            <View style={styles.badge}>
              <Text>{this.state.user.followers}</Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.followers[0].login}</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.followers[1].login}</Text>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.profilecard}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            source={require('./images/following_active.png')}
            style={{width: 20, height: 23}}
          />
          <Text
            onPress={() => this.props.navigation.navigate('Following')}
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}>Following</Text>
          <View style={styles.badge}>
            <Text>{this.state.user.following}</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.following[0].login}</Text>
        </View>
      </View>
    );
  }

  render() {
    /**
     * Check whether axios has returned data.
     */
    if (this.state.user === null || this.state.repos === null || this.state.followers === null || this.state.following === null) {
      return (
        <Loader></Loader>
      );
    };

    /**
     * Helper function to check whether the user has an email.
     */
    noEmail = () => {
      if (this.state.user.email === null) {
        return <Text>No email</Text>
      }
      return <Text
        style={styles.link}
        onPress={() => Linking.openURL('mailto:' + this.state.user.email)}>
          {this.state.user.email}
      </Text>
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.profileView}>
        <View style={{paddingTop: 20}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View>
              <Image
                style={{width: 100, height: 100, borderRadius: 4}}
                source={{uri: this.state.user.avatar_url}}
              />
            </View>
            <View style={{width: 225, marginLeft: 25, flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 20}}>{this.state.user.name}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>{this.state.user.login}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('./images/link_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                <Text
                  style={styles.link}
                  onPress={() => Linking.openURL('http://' + this.state.user.blog)}>
                    {this.state.user.blog}
                </Text>
              </Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('./images/mail_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                {noEmail()}
              </Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 120, marginBottom: 20}}>
          <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.user.bio}</Text>
        </View>
        {this.renderProfileCard('repos')}
        {this.renderProfileCard('followers')}
        {this.renderProfileCard('following')}
      </ScrollView>
    );
  }
}

class ReposView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      repos: null
    };
  }

  /**
   * Before mounting component, fetch user data.
   */
  componentDidMount() {
    const url = 'https://api.github.com/users/cristobalwee/repos';
    let repoData = null;
    axios.get(url).then(function (response) {
      repoData = response.data
    })
    .catch(function (error) {
      console.log(error);
    });
    setTimeout(() => {
      this.setState(previousState => {
        return {
          repos: repoData
        };
      });
    }, 500);
  }

  render() {
    /**
     * Helper function to check whether the repo has a description.
     */
    noDescription = (description) => {
      if (description === null) {
        return <Text>No description</Text>
      }
      return <Text>{item.description}</Text>
    };

    if (this.state.repos === null) {
      return (
        <SafeAreaView>
          <View style={styles.repos}>
            <Loader></Loader>
          </View>
        </SafeAreaView>
      );
    }
    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"}>
        <View style={styles.repos}>
          <Text style={{fontSize: 20}}>Repos: {this.state.repos.length}</Text>
          {this.state.repos.map((item, i) => {
            if (item.description === null) {
              return (
                <Text
                  style={{paddingTop: 10}}
                  onPress={() => Linking.openURL(item.html_url)}
                  key={i}><Text style={styles.link}>{item.full_name}</Text>, stars: {item.stargazers_count}
                  {"\n"}No description.
                </Text>
              );
            }
            return (
              <Text
                style={{paddingTop: 10}}
                onPress={() => Linking.openURL(item.html_url)}
                key={i}><Text style={styles.link}>{item.full_name}</Text>, stars: {item.stargazers_count}
                {"\n"}{item.description}
              </Text>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

class FollowersView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followers: null
    };
  }
  /**
   * Before mounting component, fetch user data.
   */
  componentDidMount() {
    const url = 'https://api.github.com/users/cristobalwee/followers';
    let userData = null;
    axios.get(url).then(function (response) {
      userData = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
    setTimeout(() => {
      this.setState(previousState => {
        return {
          followers: userData
        };
      });
    }, 500);
  }

  render() {
    if (this.state.followers === null) {
      return (
        <SafeAreaView>
          <View style={styles.profile}>
            <Text>Loading</Text>
          </View>
        </SafeAreaView>
      );
    }
    return (
      <SafeAreaView>
        <View style={styles.profile}>
          <Text>Followers: {this.state.followers.length}</Text>
          {this.state.followers.map((item, i) => {
            return <Text key={i}>{item.login}, repos: {item.public_repos}</Text>
          })}
        </View>
      </SafeAreaView>
    );
  }
}

class FollowingView extends React.Component {
  render() {
    return (
      <SafeAreaView>
        <View style={styles.following}>
          <Text>Following View</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const RootStack = TabNavigator(
  {
    Profile: {
      screen: ProfileView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 21, width: 20}} source={require('./images/profile_active.png')}/>
          }
          return <Image style={{height: 21, width: 20}} source={require('./images/profile_inactive.png')}/>
        },
      },
    },
    Repos: {
      screen: ReposView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 22, width: 20}} source={require('./images/repos_active.png')}/>
          }
          return <Image style={{height: 21, width: 20}} source={require('./images/repos_inactive.png')}/>
        },
      },
    },
    Followers: {
      screen: FollowersView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 20, width: 21}} source={require('./images/followers_active.png')}/>
          }
          return <Image style={{height: 20, width: 21}} source={require('./images/followers_inactive.png')}/>
        },
      },
    },
    Following: {
      screen: FollowingView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 23, width: 20}} source={require('./images/following_active.png')}/>
          }
          return <Image style={{height: 23, width: 20}} source={require('./images/following_inactive.png')}/>
        },
      },
    },
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#ffffff',
      },
      labelStyle: {
        display: 'none',
      }
    },
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  profileView: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: '8%',
    paddingRight: '8%'
  },
  repos: {
    padding: '8%',
    justifyContent: 'center'
  },
  followers: {

  },
  following: {

  },
  profilecard: {
    shadowOffset: {width: 0, height: 0},
    shadowColor: 'black',
    shadowOpacity: 0.15,
    shadowRadius: 1,
    borderRadius: 6,
    padding: 10,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'column',
    flexShrink: 0,
    height: 100
  },
  loader: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  link: {
    color: '#4a90e2'
  },
  badge: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});
