import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profile: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center'
  },
  repos: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center'
  },
  followers: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center'
  },
  following: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center'
  },
  profilecard: {
    alignItems: 'center'
  },
  loader: {
    
  }
});

export default styles;
