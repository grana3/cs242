import React from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { TabNavigator } from 'react-navigation';

import styles from './styles.jsx';

class FollowingView extends React.Component {
  render() {
    return (
      <View style={styles.following}>
        <Text>Following View</Text>
      </View>
    );
  }
}

export default FollowingView;
