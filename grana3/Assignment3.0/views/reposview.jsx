import React from 'react';
import { Button, StyleSheet, Text, View, Image, WebView } from 'react-native';
import { TabNavigator } from 'react-navigation';
import axios from 'axios';

import styles from './styles.jsx';

class ReposView extends React.Component {
  /**
   * Before mounting component, fetch user data.
   */
  componentWillMount() {
    const url = 'https://api.github.com/users/cristobalwee/repos';
    axios.get(url).then(function (response) {
      //console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    return (
      <View style={styles.repos}>
        <Text>Repos View</Text>
      </View>
    );
  }
}

export default ReposView;
