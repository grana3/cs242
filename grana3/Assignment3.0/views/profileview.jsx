import React from 'react';
import { Button, StyleSheet, Text, View, Image } from 'react-native';
import { TabNavigator } from 'react-navigation';

import styles from './styles.jsx';

class ProfileView extends React.Component {
  /**
   * Before mounting component, fetch user data.
   */
  componentWillMount() {
    const url = 'https://api.github.com/users/cristobalwee';
    axios.get(url).then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  
  render() {
    return (
      <View style={styles.profile}>
        <Text>Profile Screen</Text>
      </View>
    );
  }
}

export default ProfileView;
