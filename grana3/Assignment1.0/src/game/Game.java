package game;
import board.Board;

/**
 * This is where the game loop and GUI are to be implemented.
 * The game class will instantiate 2 players and a board.
 */
public class Game {
    Board board = new Board(8, 8);
}