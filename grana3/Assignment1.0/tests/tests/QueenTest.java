package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.Queen;

import static org.junit.jupiter.api.Assertions.*;

class QueenTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        Queen queen = new Queen(0, 5, true, "black");
        int newXCoord = 1;
        int newYCoord = 6;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = queen.makePath(board, newXCoord, newYCoord);
        Path wrongPath = queen.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}