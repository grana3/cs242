package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.Rook;

import static org.junit.jupiter.api.Assertions.*;

class RookTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        Rook rook = new Rook(0, 0, true, "black");
        int newXCoord = 0;
        int newYCoord = 6;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = rook.makePath(board, newXCoord, newYCoord);
        Path wrongPath = rook.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}