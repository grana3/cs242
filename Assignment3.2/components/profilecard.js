import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';

import styles from '../styles.js';

class ProfileCard extends React.Component {
  /**
   * Helper method to return a repo type profile card.
   */
  repoCard = () => (
    <View style={styles.profilecard}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <Image
          source={require('../images/repos_active.png')}
          style={{width: 20, height: 22}}
        />
        <Text
          style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
          onPress={() => this.props.navigation.navigate('Repos')}>
          Repos
        </Text>
        <View style={styles.badge}>
          <Text>{this.props.user.public_repos}</Text>
        </View>
      </View>
      {this.props.repos.map((repo, i) => {
        if (i < 2) {
          return (
            <View key={i} style={{flex: 1, flexDirection: 'row', paddingTop: 6}}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>{repo.full_name}</Text>
              <View style={styles.badge}>
                <Text style={{fontSize: 14, color: '#4c4c4c'}}>
                  {repo.stargazers_count}
                  <Image
                    source={require('../images/star_icon.png')}
                    style={{width: 17, height: 17, marginLeft: 5}}
                  />
                </Text>
              </View>
            </View>
          );
        }
      })}
    </View>
  );

  /**
   * Helper method to return a follower type profile card.
   */
  followerCard = () => (
    <View style={styles.profilecard} onPress={() => this.props.navigation.navigate('Followers')}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <Image
          source={require('../images/followers_active.png')}
          style={{width: 22, height: 21}}
        />
        <Text
          onPress={() => this.props.navigation.navigate('Repos')}
          style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}>Followers</Text>
        <View style={styles.badge}>
          <Text>{this.props.user.followers}</Text>
        </View>
      </View>
      {this.props.followers.map((follower, i) => {
        if (i < 2) {
          return (
            <View key={i} style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>{follower.login}</Text>
            </View>
          );
        }
      })}
    </View>
  );

  /**
   * Helper method to return a following type profile card.
   */
  followingCard = () => (
    <View style={styles.profilecard}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <Image
          source={require('../images/following_active.png')}
          style={{width: 20, height: 23}}
        />
        <Text
          onPress={() => this.props.navigation.navigate('Following')}
          style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}>Following</Text>
        <View style={styles.badge}>
          <Text>{this.props.user.following}</Text>
        </View>
      </View>
      {this.props.following.map((following, i) => {
        if (i < 2) {
          return (
            <View key={i} style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{fontSize: 14, color: '#4c4c4c'}}>{following.login}</Text>
            </View>
          );
        }
      })}
    </View>
  );

  render() {
    if (this.props.type === 'repos') {
      return this.repoCard();
    } else if (this.props.type === 'followers') {
      return this.followerCard();
    }

    return this.followingCard();
  }
}

export default ProfileCard;
