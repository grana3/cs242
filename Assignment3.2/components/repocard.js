import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';

import styles from '../styles.js';

class RepoCard extends React.Component {
  displayDescription = (noDescription) => {
    if (noDescription) {
      return <Text>No description.</Text>;
    }
    if (this.props.repo.description.length > 100) {
      return <Text>{this.props.repo.description.slice(0, 100)}...</Text>;
    }
    return <Text>{this.props.repo.description}</Text>;
  }

  render() {
    return (
      <View style={styles.repocard}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            source={require('../images/repos_active.png')}
            style={{width: 20, height: 22}}
          />
          <Text
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
            onPress={() => Linking.openURL(this.props.repo.html_url)}>
            {this.props.repo.name}
          </Text>
          <View style={styles.badge}>
            <Text style={{fontSize: 14, color: '#4c4c4c'}}>
              {this.props.repo.stargazers_count}
              <Image
                source={require('../images/star_icon.png')}
                style={{width: 17, height: 17, marginLeft: 5}}
              />
            </Text>
          </View>
        </View>
        <Text>{this.displayDescription(this.props.noDescription)}</Text>
      </View>
    );
  }
}

export default RepoCard;
