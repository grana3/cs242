import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import Error from '../components/error.js';
import RepoCard from '../components/repocard.js';
import FollowerCard from '../components/followercard.js';

class SearchResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      resultCount: 0,
      queryType: ''
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const query = params ? params.query : null;
    const type = params ? params.type : null;
    let resultData = null;
    let count = 0;

    if (type === 'repos') {
      axios.get('https://api.github.com/search/repositories?q=' + query).then((response) => {
        resultData = response.data.items;
        count = response.data.total_count;
      })
      .catch((error) => {
        console.log(error);
      });
    } else if (type === 'users') {
      axios.get('https://api.github.com/search/users?q=' + query).then((response) => {
        resultData = response.data.items;
        count = response.data.total_count;
      })
      .catch((error) => {
        console.log(error);
      });
    }

    setTimeout(() => {
      this.setState(previousState => {
        return {
          results: resultData,
          resultCount: count,
          queryType: type
        };
      });
    }, 500);
  }

  render() {
    /**
     * Check whether axios has returned data.
     */
    if (this.state.results === null || this.state.results.length === 0) {
      return (
        <Loader></Loader>
      );
    };

    if (this.state.queryType === 'users') {
      return (
        <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
          <Text style={{fontSize: 28, marginBottom: 20, paddingTop: 20}}>
            Results &nbsp;
            <Text style={{fontSize: 14, paddingBottom: 5}}>
              {this.state.resultCount}
            </Text>
          </Text>
          {this.state.results.map((item, i) => {
            return (
              <FollowerCard key={i} follower={item} navigation={this.props.navigation} />
            );
          })}
        </ScrollView>
      );
    }

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        <Text style={{fontSize: 28, marginBottom: 20, paddingTop: 20}}>
          Results &nbsp;
          <Text style={{fontSize: 14, paddingBottom: 5}}>
            {this.state.resultCount}
          </Text>
        </Text>
        {this.state.results.map((item, i) => {
          if (item.description === null) {
            return (
              <RepoCard key={i} repo={item} noDescription />
            );
          }
          return (
            <RepoCard key={i} repo={item} />
          );
        })}
      </ScrollView>
    );
  }
}

export default SearchResults;
