import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView, TouchableHighlight } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import ProfileCard from '../components/profilecard.js';
import Error from '../components/error.js';
import AnimatedBar from '../components/animatedbar.js';

class Visualization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      commits: []
    }
  }

  componentDidMount() {
    let commitData = null;

    axios.get('https://api.github.com/repos/facebook/react/contributors').then((response) => {
      commitData = response.data;
    })
    .catch((error) => {
      console.log(error);
    });

    /**
     * Wait for data to return, then set state accordingly.
     */
    setTimeout(() => {
      this.setState(previousState => {
        return {
          commits: commitData
        };
      });
    }, 500);
  }

  render() {
    /**
     * Check whether axios has returned data.
     */
    if (this.state.commits.length === 0) {
      return (
        <Loader></Loader>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        <View style={{flex: 1, flexDirection: 'row', paddingTop: 20}}>
          <Text style={{fontSize: 28, marginBottom: 20}}>Contributors &nbsp;
            <Text style={{fontSize: 14, paddingBottom: 5}}>
              {this.state.commits.length}
            </Text>
          </Text>
        </View>
        {this.state.commits.map((item, i) => {
          return (
            <View key={i} style={{
                flex: 1,
                flexDirection: 'column',
                paddingBottom: 10,
                shadowOffset: {width: 0, height: 0},
                shadowColor: 'black',
                shadowOpacity: 0.15,
                shadowRadius: 1,
                borderRadius: 6,
                padding: 10,
                marginBottom: 10,
              }}>
              <Text style={{paddingBottom: 10}}>{item.login}</Text>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <AnimatedBar value={item.contributions/7} delay={50 * i} />
                <Text>&nbsp; {item.contributions}</Text>
              </View>
            </View>
          );
        })}
      </ScrollView>
    );
  }
}

export default Visualization;
