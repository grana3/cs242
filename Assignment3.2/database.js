const Realm = require('realm');

const UserSchema = {
  name: 'User',
  properties: {
    name: 'string',
    userName:  'string',
    url: 'string',
    bio: 'string',
    repos: '[]',
    followers: '[]',
    following: '[]'
  }
};
