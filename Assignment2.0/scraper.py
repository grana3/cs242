import urllib2
from bs4 import BeautifulSoup
from collections import defaultdict

# Create our dictionary of actors
directory = defaultdict(list)

# Define the starting URL and parse the page
startUrl = "https://en.wikipedia.org/wiki/Brad_Pitt"
page = urllib2.urlopen(startUrl)
soup = BeautifulSoup(page, "html.parser")

# Get the page's heading, corresponding to the name of the actor or movie
nameAttr = soup.find("h1", attrs={"id": "firstHeading"})
actorName = nameAttr.text.strip()

filmographyAttr = soup.find("span", attrs={"id": "Filmography"})
# Some articles have "Filmography" listed as "Filmography (selected)"
if filmographyAttr is None:
    filmographyAttr = soup.find("span", attrs={"id": "Filmography_(selected)"})

note = filmographyAttr.find_next()
filmsUrl = note.find_next()
filmography = filmsUrl.find_next()

print(actorName)
print(filmography.text.strip())
print(directory.items())

# https://gist.github.com/anirudhjayaraman/1f74eb656c3dd85ff440fb9f9267f70a