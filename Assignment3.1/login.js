import React from 'react';
import { Button, Text, View, Image, TextInput, TouchableHighlight } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import ProfileView from './views/profileview.js';
import ReposView from './views/reposview.js';
import FollowersView from './views/followersview.js';
import FollowingView from './views/followingview.js';
import styles from './styles.js';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userText: 'Username',
      password: 'Password'
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.login}>
        <Image
          style={{width: 75, height: 75}}
          source={require('./images/logo.png')}
        />
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({userText: text})}
          value={this.state.userText}
        />
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          onChangeText={(text) => this.setState({password: text})}
          value={this.state.password}
        />
        <Text>{'\n'}</Text>
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate('SignedIn', {userName: this.state.userText.toLowerCase(), password: this.state.password})}>
          <Image
            style={{width: 158, height: 46}}
            source={require('./images/login_button.png')}
          />
        </TouchableHighlight>
      </SafeAreaView>
    );
  }
}

export default Login;
