import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';

class FollowerCard extends React.Component {
  render() {
    const oauthToken = '16fd932169decd4cfc2b';

    following = () => {
      if (this.props.following) {
        return (
          <Text
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
            onPress={() => {
              axios.delete('/user/following/' + this.props.follower.login, null, {
                  baseURL: 'https://api.github.com',
                  headers: { 'Authorization': 'token ' + oauthToken,
                              'Content-Length': 0 }
              })
              .then(function (response) {
            		console.log(response);
              })
              .catch(function (error) {
                console.log(error);
              });
            }}>
            Unfollow
          </Text>
        );
      }

      return (
        <Text
          style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
          onPress={() => {
            axios.put('/user/following/' + this.props.follower.login, null, {
                baseURL: 'https://api.github.com',
                headers: { 'Authorization': 'token ' + oauthToken,
                            'Content-Length': 0 }
            })
            .then(function (response) {
          		console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            });
          }}>
          Follow
        </Text>
      );
    }
    return (
      <View style={styles.repocard}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            source={{uri: this.props.follower.avatar_url}}
            style={{width: 36, height: 36, borderRadius: 4}}
          />
          <Text
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
            onPress={() => this.props.navigation.navigate('Profile', {userName: this.props.follower.login})}>
            {this.props.follower.login}
          </Text>
        </View>
        {following()}
      </View>
    );
  }
}

export default FollowerCard;
