import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import ProfileCard from '../components/profilecard.js';
import Error from '../components/error.js'

class ExternalProfileView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      repos: [],
      followers: [],
      following: [],
      networkError: false
    };
  }

  /**
   * Before mounting component, fetch user, repo, and follower/following data.
   */
  componentDidMount() {
    const { params } = this.props.navigation.state;
    const userName = params ? params.userName : null;
    const url = 'https://api.github.com/users/' + userName;
    let userData = null;
    let repoData = null;
    let followerData = null;
    let followingData = null;
    let error = false;

    axios.get(url).then(function (response) {
      userData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/repos').then(function (response) {
      repoData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/followers').then(function (response) {
      followerData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/following').then(function (response) {
      followingData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    /**
     * Wait for data to return, then set state accordingly.
     */
    setTimeout(() => {
      this.setState(previousState => {
        return {
          user: userData,
          repos: repoData,
          followers: followerData,
          following: followingData,
          networkError: error
        };
      });
    }, 500);
  }

  render() {
    /**
     * Check for network error.
     */
    if (this.state.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.state.user === null || this.state.repos === null || this.state.followers === null || this.state.following === null) {
      return (
        <Loader></Loader>
      );
    };

    /**
     * Helper function to check whether the user has an email.
     */
    noEmail = () => {
      if (this.state.user.email === null) {
        return <Text>No email</Text>
      }
      return (
        <Text
          style={styles.link}
          onPress={() => Linking.openURL('mailto:' + this.state.user.email)}>
            {this.state.user.email}
        </Text>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.profileView}>
        <View style={{paddingTop: 20}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View>
              <Image
                style={{width: 100, height: 100, borderRadius: 4}}
                source={{uri: this.state.user.avatar_url}}
              />
            </View>
            <View style={{width: 225, marginLeft: 25, flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 20}}>{this.state.user.name}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>{this.state.user.login}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('../images/link_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                <Text
                  style={styles.link}
                  onPress={() => Linking.openURL('http://' + this.state.user.blog)}>
                    {this.state.user.blog}
                </Text>
              </Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('../images/mail_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                {noEmail()}
              </Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 20, marginBottom: 20}}>
          <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.state.user.bio}</Text>
        </View>
        <ProfileCard
          navigation={this.props.navigation}
          type={"repos"}
          user={this.state.user}
          repos={this.state.repos}
          followers={this.state.followers}
          following={this.state.following}
        />
        <ProfileCard
          navigation={this.props.navigation}
          type={"followers"}
          user={this.state.user}
          repos={this.state.repos}
          followers={this.state.followers}
          following={this.state.following}
        />
        <ProfileCard
          navigation={this.props.navigation}
          type={"following"}
          user={this.state.user}
          repos={this.state.repos}
          followers={this.state.followers}
          following={this.state.following}
        />
      </ScrollView>
    );
  }
}

export default ExternalProfileView;
