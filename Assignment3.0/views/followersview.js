import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { StackNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import Error from '../components/error.js';
import FollowerCard from '../components/followercard.js';
import ExternalProfileView from './externalprofileview.js';

class Followers extends React.Component {
  render() {
    /**
     * Check for network error.
     */
    if (this.props.screenProps.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.props.screenProps.followers === null) {
      return (
        <Loader></Loader>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.reposView}>
        <Text style={{fontSize: 28, marginBottom: 20, paddingTop: 20}}>Followers &nbsp;
          <Text style={{fontSize: 14, paddingBottom: 5}}>
            {this.props.screenProps.followers.length}
          </Text>
        </Text>
        {this.props.screenProps.followers.map((item, i) => {
          return (
            <FollowerCard key={i} follower={item} navigation={this.props.navigation} />
          );
        })}
      </ScrollView>
    );
  }
}

const RootStack = StackNavigator(
  {
    Followers: {
      screen: Followers,
    },
    Profile: {
      screen: ExternalProfileView,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Followers',
  }
);

class FollowersView extends React.Component {
  render() {
    return <RootStack screenProps={this.props.screenProps}/>;
  }
}

export default FollowersView;
