import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import styles from '../styles.js';
import Loader from '../components/loader.js';
import ProfileCard from '../components/profilecard.js';
import Error from '../components/error.js'

class ProfileView extends React.Component {
  render() {
    /**
     * Check for network error.
     */
    if (this.props.screenProps.networkError) {
      return (
        <Error></Error>
      );
    }

    /**
     * Check whether axios has returned data.
     */
    if (this.props.screenProps.user === null || this.props.screenProps.repos === null || this.props.screenProps.followers === null || this.props.screenProps.following === null) {
      return (
        <Loader></Loader>
      );
    };

    /**
     * Helper function to check whether the user has an email.
     */
    noEmail = () => {
      if (this.props.screenProps.user.email === null) {
        return <Text>No email</Text>
      }
      return (
        <Text
          style={styles.link}
          onPress={() => Linking.openURL('mailto:' + this.props.screenProps.user.email)}>
            {this.props.screenProps.user.email}
        </Text>
      );
    };

    return (
      <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.profileView}>
        <View style={{paddingTop: 20}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View>
              <Image
                style={{width: 100, height: 100, borderRadius: 4}}
                source={{uri: this.props.screenProps.user.avatar_url}}
              />
            </View>
            <View style={{width: 225, marginLeft: 25, flex: 1, flexDirection: 'column'}}>
              <Text style={{fontSize: 20}}>{this.props.screenProps.user.name}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>{this.props.screenProps.user.login}</Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('../images/link_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                <Text
                  style={styles.link}
                  onPress={() => Linking.openURL('http://' + this.props.screenProps.user.blog)}>
                    {this.props.screenProps.user.blog}
                </Text>
              </Text>
              <Text style={{fontSize: 14, marginTop: 5, color: '#4c4c4c'}}>
                <Image
                  source={require('../images/mail_icon.png')}
                  style={{width: 10, height: 7}}
                />&nbsp;
                {noEmail()}
              </Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 20, marginBottom: 20}}>
          <Text style={{fontSize: 14, color: '#4c4c4c'}}>{this.props.screenProps.user.bio}</Text>
        </View>
        <ProfileCard
          navigation={this.props.navigation}
          type={"repos"}
          user={this.props.screenProps.user}
          repos={this.props.screenProps.repos}
          followers={this.props.screenProps.followers}
          following={this.props.screenProps.following}
        />
        <ProfileCard
          navigation={this.props.navigation}
          type={"followers"}
          user={this.props.screenProps.user}
          repos={this.props.screenProps.repos}
          followers={this.props.screenProps.followers}
          following={this.props.screenProps.following}
        />
        <ProfileCard
          navigation={this.props.navigation}
          type={"following"}
          user={this.props.screenProps.user}
          repos={this.props.screenProps.repos}
          followers={this.props.screenProps.followers}
          following={this.props.screenProps.following}
        />
        <Text onPress={() => this.props.screenProps.superNavigation.navigate('SignedOut')}>Log out</Text>
      </ScrollView>
    );
  }
}

export default ProfileView;
