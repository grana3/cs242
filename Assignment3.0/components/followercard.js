import React from 'react';
import { Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';

import styles from '../styles.js';

class FollowerCard extends React.Component {
  render() {
    return (
      <View style={styles.repocard}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            source={{uri: this.props.follower.avatar_url}}
            style={{width: 36, height: 36, borderRadius: 4}}
          />
          <Text
            style={{color: '#4a90e2', fontSize: 16, marginLeft: 10}}
            onPress={() => this.props.navigation.navigate('Profile', {userName: this.props.follower.login})}>
            {this.props.follower.login}
          </Text>
        </View>
      </View>
    );
  }
}

export default FollowerCard;
