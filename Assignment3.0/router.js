import React from 'react';
import { AsyncStorage, Button, StyleSheet, Text, View, Image, Linking, ScrollView } from 'react-native';
import { TabNavigator, SafeAreaView } from 'react-navigation';
import axios from 'axios';

import ProfileView from './views/profileview.js';
import ReposView from './views/reposview.js';
import FollowersView from './views/followersview.js';
import FollowingView from './views/followingview.js';
import styles from './styles.js';

/**
 * This is the component where the navigation logic is implemented after log in.
 */
const RootStack = TabNavigator(
  {
    Profile: {
      screen: ProfileView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 21, width: 20}} source={require('./images/profile_active.png')}/>
          }
          return <Image style={{height: 21, width: 20}} source={require('./images/profile_inactive.png')}/>
        },
      },
    },
    Repos: {
      screen: ReposView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 22, width: 20}} source={require('./images/repos_active.png')}/>
          }
          return <Image style={{height: 21, width: 20}} source={require('./images/repos_inactive.png')}/>
        },
      },
    },
    Followers: {
      screen: FollowersView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 20, width: 21}} source={require('./images/followers_active.png')}/>
          }
          return <Image style={{height: 20, width: 21}} source={require('./images/followers_inactive.png')}/>
        },
      },
    },
    Following: {
      screen: FollowingView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Image style={{height: 23, width: 20}} source={require('./images/following_active.png')}/>
          }
          return <Image style={{height: 23, width: 20}} source={require('./images/following_inactive.png')}/>
        },
      },
    },
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#ffffff',
      },
      labelStyle: {
        display: 'none',
      }
    },
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

class Router extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      repos: [],
      followers: [],
      following: [],
      networkError: false
    };
  }

  /**
   * Before mounting component, fetch user, repo, and follower/following data.
   */
  componentDidMount() {
    const { params } = this.props.navigation.state;
    const userName = params ? params.userName : null;
    const password = params ? params.password : null;
    try {
      AsyncStorage.setItem('userName', userName);
      AsyncStorage.setItem('password', password);
    } catch (error) {
      console.log("There was an error saving data");
    }
    const url = 'https://api.github.com/users/' + userName;
    let userData = null;
    let repoData = null;
    let followerData = null;
    let followingData = null;
    let error = false;

    axios.get(url).then(function (response) {
      userData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/repos').then(function (response) {
      repoData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/followers').then(function (response) {
      followerData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    axios.get('https://api.github.com/users/' + userName + '/following').then(function (response) {
      followingData = response.data;
      error = false;
    })
    .catch(function (error) {
      console.log(error);
      error = true;
    });

    /**
     * Wait for data to return, then set state accordingly.
     */
    setTimeout(() => {
      this.setState(previousState => {
        return {
          user: userData,
          repos: repoData,
          followers: followerData,
          following: followingData,
          networkError: error
        };
      });
    }, 500);
  }

  render() {
    return (
      <RootStack
        screenProps={{
          user: this.state.user,
          repos: this.state.repos,
          followers: this.state.followers,
          following: this.state.following,
          networkError: this.state.networkError,
          superNavigation: this.props.navigation
        }}/>
    );
  }
}

export default Router;
