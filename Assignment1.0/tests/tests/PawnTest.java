package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.Pawn;

import static org.junit.jupiter.api.Assertions.*;

class PawnTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        Pawn pawn = new Pawn(0, 5, true, "black");
        int newXCoord = 1;
        int newYCoord = 5;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = pawn.makePath(board, newXCoord, newYCoord);
        Path wrongPath = pawn.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}