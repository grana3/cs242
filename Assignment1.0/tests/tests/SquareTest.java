package tests;

import board.Square;
import org.junit.jupiter.api.Test;
import piece.Piece;
import piece.Rook;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test
    void getRowCoord() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, rook);

        assertEquals(0, square.getRowCoord());
    }

    @Test
    void getColCoord() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, rook);

        assertEquals(0, square.getColCoord());
    }

    @Test
    void getPiece() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, rook);

        assertEquals(rook, square.getPiece());
    }

    @Test
    void isAvailable() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, rook);

        assertFalse(square.isAvailable());
        square.vacate();

        assertTrue(square.isAvailable());
    }

    @Test
    void occupy() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, null);

        square.occupy(rook);
        assertEquals(rook, square.getPiece());
    }

    @Test
    void vacate() {
        Rook rook = new Rook(0, 0, true, "white");
        Square square = new Square(0, 0, rook);

        square.vacate();
        assertNull(square.getPiece());
    }
}