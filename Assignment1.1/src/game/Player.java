package game;

/**
 * This class will take care of the turn dynamic and score.
 */
public class Player {
    private final String color;
    private boolean inPlay;

    public Player(String color, boolean inPlay) {
        this.color = color;
        this.inPlay = inPlay;
    }

    public String getColor() {
        return this.color;
    }

    public boolean isInPlay() {
        return this.inPlay;
    }
}