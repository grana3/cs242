package game;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class contains the new game and quit buttons.
 */
public class ControlPanel extends JPanel {
    public ControlPanel() {
        JButton quit = new JButton("Quit");
        JButton newGame = new JButton("New Game");
        JButton undo = new JButton("Undo");
        JLabel turn = new JLabel("White's turn");
        JLabel score = new JLabel("White: 0, Black: 0");
        add(turn);
        add(quit);
        add(newGame);
        add(undo);
        add(score);

        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Click ok to start a new game");
            }
        });

        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Are you sure you want to quit?");
            }
        });

        this.setVisible(true);
    }
}
