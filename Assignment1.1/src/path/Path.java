package path;

import java.util.*;

/**
 * The path class serves to determine whether a piece's move is legal without needing to jump over other piece.
 * A path object comprises an arraylist of int arrays corresponding to coordinates, and a boolean value.
 */
public class Path {
    public boolean valid;
    public List<int[]> coordinates;

    public Path() {
        this.valid = false;
        this.coordinates = new ArrayList<int[]>();
    }

    public void addCoords(int rowCoord, int colCoord) {
        int[] coords = {rowCoord, colCoord};
        this.coordinates.add(coords);
    }

    public boolean isValid() {
        return this.valid;
    }

    public List<int[]> getCoordinates() {
        return this.coordinates;
    }

    public void makeValid() {
        this.valid = true;
    }
}