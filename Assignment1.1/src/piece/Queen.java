package piece;

import path.Path;
import board.Board;

public class Queen extends Piece {
    public Queen(int x, int y, boolean playable, String color) {
        super(x, y, playable, color);
    }

    /**
     * This method creates a path object with the coordinates between a piece and a given location
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Path from the piece's current coordinates to those given.
     */
    @Override
    public Path makePath(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        // Instantiate a path.Path object
        Path path = new Path();
        if (!isLegalMove(board, newRowCoord, newColCoord)) {
            return path;
        }

        if (newRowCoord == rowCoord) {
            super.makeVerticalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);
        }
        if (newColCoord == colCoord) {
            super.makeHorizontalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);
        }
        if (newRowCoord - rowCoord == newColCoord - colCoord) {
            super.makeDiagonalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);
        }
        
        path.makeValid();

        return path;
    }

    /**
     * This method creates a path object with the coordinates between a piece and a given location
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return boolean Whether the move is legal for the current piece.
     */
    @Override
    public boolean isLegalMove(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        // Check whether the move fits the piece's constraints
        if (newRowCoord - rowCoord == newColCoord - colCoord) {
            return true;
        }
        if (newRowCoord == rowCoord) {
            return true;
        }
        if (newColCoord == colCoord) {
            return true;
        }

        return false;
    }
}