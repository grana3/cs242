package tests;

import org.junit.jupiter.api.Test;
import path.Path;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PathTest {

    @Test
    void addsGetsCoords() {
        Path path = new Path();
        path.addCoords(0, 0);
        List<int[]> coordinates = path.getCoordinates();

        assertNotNull(coordinates);
        assertEquals(1, coordinates.size());
    }

    @Test
    void isMakesValid() {
        Path path = new Path();

        assertFalse(path.isValid());
        path.makeValid();

        assertTrue(path.isValid());
    }
}