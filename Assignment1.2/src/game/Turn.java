package game;

/**
 * Turn enum to keep track of which player's turn it is.
 */
public enum Turn {
    WHITE, BLACK
}
