package game;

import board.Board;
import board.Square;
import javafx.scene.shape.Arc;
import piece.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * This class acts as the graphical representation of our chess board.
 * Piece images and colors taken from https://en.wikipedia.org/wiki/Chess_piece.
 */
public class ChessBoard extends JPanel {
    private final int rows;
    private final int columns;
    private final String PATH = "/Users/cristobalgrana/Documents/cs242/grana3/Assignment1.2/src/media/";
    public JButton[][] spots;
    public Board logicBoard;
    public List<int[]> buttonsPressed;
    public List<int[]> moves;
    public Turn turn;
    public LabelPanel labels;
    public Piece takenPiece;
    private boolean enabled;

    public ChessBoard(int rows, int columns, Turn turn, LabelPanel labels) {
        this.rows = rows;
        this.columns = columns;
        this.spots = new JButton[rows][columns];
        this.buttonsPressed = new ArrayList<int[]>();
        this.moves = new ArrayList<int[]>();
        this.turn = turn;
        this.labels = labels;
        this.enabled = true;

        Color black = new Color(200, 141, 83);
        Color white = new Color(248, 207, 164);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j % 2 == 0) {
                    JButton button = new JButton();
                    paintButton(white, button);
                    add(button);
                    this.spots[i][j] = button;
                } else {
                    JButton button = new JButton();
                    paintButton(black, button);
                    add(button);
                    this.spots[i][j] = button;
                }

                if (j == 7) {
                    Color swapper = white;
                    white = black;
                    black = swapper;
                }
            }
        }

        this.setLayout(new GridLayout(8, 8));
        this.setSize(600, 600);
        this.setVisible(true);
    }

    /**
     * Helper function to set a button's color and visibility.
     * @param color The color to paint the button.
     * @param button The button to paint.
     */
    private void paintButton(Color color, JButton button) {
        button.setOpaque(true);
        button.setBackground(color);
        button.setBorderPainted(false);
    }

    /**
     * Analog to method init() in the Board class.
     * Also used to refresh the board after a piece has moved.
     */
    public void init() {
        this.logicBoard = new Board(this.rows, this.columns);
        this.logicBoard.init();
        this.refreshBoard();
        Square[][] cells = this.logicBoard.getCells();

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                int rowCoord = i;
                int colCoord = j;
                List<int[]> buttonsPressed = this. buttonsPressed;
                this.spots[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        int[] newCoords = {rowCoord, colCoord};
                        if (!enabled) {
                            return;
                        }
                        if (!buttonsPressed.isEmpty()) {
                            int[] currCoords = buttonsPressed.get(0);
                            if (currCoords[0] == rowCoord && currCoords[1] == colCoord) {
                                return;
                            }
                            if (cells[rowCoord][colCoord].getPiece() != null) {
                                takenPiece = cells[rowCoord][colCoord].getPiece();
                            }
                            movePiece(currCoords[0], currCoords[1], rowCoord, colCoord);
                            moves.add(0, buttonsPressed.get(0));
                            moves.add(1, newCoords);
                            buttonsPressed.clear();
                            changeTurn();
                        } else {
                            if (cells[rowCoord][colCoord].getPiece() == null) {
                                return;
                            }
                            if (turn == Turn.WHITE && cells[rowCoord][colCoord].getPiece().getColor() != "white") {
                                return;
                            }
                            if (turn == Turn.BLACK && cells[rowCoord][colCoord].getPiece().getColor() != "black") {
                                return;
                            }
                            buttonsPressed.add(newCoords);
                            takenPiece = null;
                        }
                    }
                });
            }
        }
    }

    /**
     * Loop over the model board and update the pieces in the view accordingly.
     */
    private void refreshBoard() {
        Square[][] cells = this.logicBoard.getCells();

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                if (cells[i][j].getPiece() instanceof Pawn) {
                    setImage(cells, i, j, "white_pawn.png", "black_pawn.png");

                } else if (cells[i][j].getPiece() instanceof Rook) {
                    setImage(cells, i, j, "white_rook.png", "black_rook.png");

                } else if (cells[i][j].getPiece() instanceof Knight) {
                    setImage(cells, i, j, "white_knight.png", "black_knight.png");

                } else if (cells[i][j].getPiece() instanceof Bishop) {
                    setImage(cells, i, j, "white_bishop.png", "black_bishop.png");

                } else if (cells[i][j].getPiece() instanceof Queen) {
                    setImage(cells, i, j, "white_queen.png", "black_queen.png");

                } else if (cells[i][j].getPiece() instanceof King) {
                    setImage(cells, i, j, "white_king.png", "black_king.png");

                } else if (cells[i][j].getPiece() instanceof Commoner) {
                    setImage(cells, i, j, "white_commoner.png", "black_commoner.png");

                } else if (cells[i][j].getPiece() instanceof Archbishop) {
                    setImage(cells, i, j, "white_archbishop.png", "black_archbishop.png");

                }

            }
        }
    }

    /**
     * Helper function to set a button's background image.
     * @param cells The array of squares representing the board.
     * @param i The row coordinate of the square.
     * @param j The column coordinate of the square.
     * @param whitePiece The white image of the corresponding piece.
     * @param blackPiece The black image of the corresponding piece.
     */
    private void setImage(Square[][] cells, int i, int j, String whitePiece, String blackPiece) {
        ImageIcon img = null;
        if (cells[i][j].getPiece().getColor() == "white") {
            img = new ImageIcon(PATH + whitePiece);
        } else {
            img = new ImageIcon(PATH + blackPiece);
        }
        this.spots[i][j].setIcon(img);
    }

    /**
     * Moves the piece from the initial coordinates to new coordinates.
     * @param rowCoord Given row coordinate.
     * @param colCoord Given column coordinate.
     * @param newRowCoord Destination row coordinate.
     * @param newColCoord Destination column coordinate.
     */
    public void movePiece(int rowCoord, int colCoord, int newRowCoord, int newColCoord) {
        Square[][] cells = this.logicBoard.getCells();
        boolean moveSuccess = this.logicBoard.movePiece(cells[rowCoord][colCoord], cells[newRowCoord][newColCoord]);

        if (!moveSuccess) {
            JOptionPane.showMessageDialog(null, "Invalid move");
            this.changeTurn();
            return;
        }

        if (this.logicBoard.whiteInCheck()) {
            if (this.logicBoard.whiteInCheckMate()) {
                JOptionPane.showMessageDialog(null, "White is in checkmate, Black wins");
                labels.blackWins();
                this.enabled = false;
            } else {
                JOptionPane.showMessageDialog(null, "White is in check");
            }
        }
        if (this.logicBoard.blackInCheck()) {
            if (this.logicBoard.blackInCheckMate()) {
                JOptionPane.showMessageDialog(null, "Black is in checkmate, White wins");
                labels.whiteWins();
                this.enabled = false;
            } else {
                JOptionPane.showMessageDialog(null, "Black is in check");
            }
        }

        this.refreshBoard();
        this.spots[rowCoord][colCoord].setIcon(null);
    }

    /**
     * Undoes last move.
     */
    public void undoMove() {
        boolean pieceWasTaken = false;
        if (this.moves.isEmpty()) {
            return;
        }
        Square[][] cells = this.logicBoard.getCells();

        int initialRow = this.moves.get(0)[0];
        int initialCol = this.moves.get(0)[1];
        int destinationRow = this.moves.get(1)[0];
        int destinationCol = this.moves.get(1)[1];

        this.logicBoard.undoMove(cells[destinationRow][destinationCol], cells[initialRow][initialCol]);
        if (takenPiece != null) {
            takenPiece.setInPlay();
            cells[destinationRow][destinationCol].occupy(takenPiece);
            takenPiece = null;
            pieceWasTaken = true;
        }
        this.moves.clear();
        this.refreshBoard();
        this.changeTurn();
        if (!pieceWasTaken) {
            this.spots[destinationRow][destinationCol].setIcon(null);
        }
    }

    /**
     * Switches the turn on the board and the labels.
     */
    private void changeTurn() {
        if (turn == Turn.WHITE) {
            turn = Turn.BLACK;
        } else {
            turn = Turn.WHITE;
        }
        labels.changeTurn();
    }

    /**
     * Reset the board state.
     */
    public void newGame() {
        this.logicBoard = new Board(this.rows, this.columns);
        this.logicBoard.init();

        this.buttonsPressed.clear();
        this.moves.clear();
        if (turn != Turn.WHITE) {
            turn = Turn.WHITE;
            labels.changeTurn();
        }

        this.resetImages();
        this.refreshBoard();
        this.enabled = true;
    }

    /**
     * Reset button images
     */
    private void resetImages() {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                this.spots[i][j].setIcon(null);
            }
        }
    }
}
