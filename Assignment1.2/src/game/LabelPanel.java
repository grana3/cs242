package game;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * JPanel class that comprises a turn label and a score label.
 */
public class LabelPanel extends JPanel {
    public Turn turn;
    // White's score corresponds to score[0], Black's to score[1].
    private int[] score = {0, 0};
    private JLabel turnLabel = null;
    private JLabel scoreLabel = null;

    public LabelPanel(Turn turn) {
        this.turn = turn;

        if (turn == Turn.WHITE) {
            this.turnLabel = new JLabel("White's turn | ");
        } else {
            this.turnLabel = new JLabel("Black's turn | ");
        }

        this.scoreLabel = new JLabel("White: " + this.score[0] + ", Black: " + this.score[1]);
        add(this.turnLabel);
        add(this.scoreLabel);

        this.setVisible(true);
    }

    /**
     * Sets the text detailing whose turn it is.
     */
    public void changeTurn() {
        if (turn == Turn.WHITE) {
            turn = Turn.BLACK;
            this.turnLabel.setText("Black's turn | ");
        } else {
            turn = Turn.WHITE;
            this.turnLabel.setText("White's turn | ");
        }

    }

    public void whiteWins() {
        this.score[0] += 1;
        this.scoreLabel.setText("White: " + this.score[0] + ", Black: " + this.score[1]);
    }

    public void blackWins() {
        this.score[1] += 1;
        this.scoreLabel.setText("White: " + this.score[0] + ", Black: " + this.score[1]);
    }

    public int getWhiteScore() {
        return this.score[0];
    }

    public int getBlackScore() {
        return this.score[1];
    }
}
