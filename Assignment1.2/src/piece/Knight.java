package piece;

import path.Path;
import board.Board;

public class Knight extends Piece {
    public Knight(int x, int y, boolean playable, String color) {
        super(x, y, playable, color);
    }

    /**
     * This method creates a path object with the coordinates between a piece and a given location
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Path from the piece's current coordinates to those given.
     */
    @Override
    public Path makePath(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        // Instantiate a path.Path object
        Path path = new Path();
        if (!isLegalMove(board, newRowCoord, newColCoord)) {
            return path;
        }

        path.makeValid();

        return path;
    }

    /**
     * This method checks whether a move to the given coordinates is legal.
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return boolean Whether the move is legal for the current piece.
     */
    @Override
    public boolean isLegalMove(Board board, int newRowCoord, int newColCoord) {
        // Get the piece's current coordinates
        int rowCoord = super.getRowCoord();
        int colCoord = super.getColCoord();

        // Check whether the move fits the piece's constraints
        if (newRowCoord != rowCoord + 1 && newRowCoord != rowCoord - 1 && newRowCoord != rowCoord + 2 && newRowCoord != rowCoord - 2) {
            return false;
        }

        if (newColCoord != colCoord + 1 && newColCoord != colCoord - 1 && newColCoord != colCoord + 2 && newColCoord != colCoord - 2) {
            return false;
        }

        return true;
    }
}