package piece;

import board.Board;
import path.Path;

/**
 * All piece inherit from this class.
 * The abstract methods isLegalMove() and makePath() are to be implemented by inherited classes.
 */
public abstract class Piece {
    private int rowCoord;
    private int colCoord;
    // Boolean value determining whether the piece is on the board
    private boolean inPlay;
    private final String color;
    // To be implemented by each individual piece according to their available moves
    public abstract boolean isLegalMove(Board board, int newRowCoord, int newColCoord);
    public abstract Path makePath(Board board, int newRowCoord, int newColCoord);

    public Piece(int x, int y, boolean playable, String color) {
        super();
        this.rowCoord = x;
        this.colCoord = y;
        this.inPlay = playable;
        this.color = color;
    }

    public int getRowCoord() {
        return this.rowCoord;
    }

    public int getColCoord() {
        return this.colCoord;
    }

    public String getColor() {
        return this.color;
    }

    public boolean isInPlay() {
        return this.inPlay;
    }

    public void setRowCoord(int x) {
        this.rowCoord = x;
    }

    public void setColCoord(int y) {
        this.colCoord = y;
    }

    public void deletePiece() {
        this.inPlay = false;
    }

    public void setInPlay() {
        this.inPlay = true;
    }

    /**
     * Check whether a move is within the board.
     * @param board The board in which the piece is to move
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return boolean Whether the move is within the board's boundaries.
     */
    public boolean movesWithinBoard(Board board, int newRowCoord, int newColCoord) {
        if (newRowCoord < 0 || newColCoord < 0) {
            return false;
        }
        if (newRowCoord >= board.cells.length || newColCoord >= board.cells[0].length) {
            return false;
        }

        return true;
    }

    /**
     * Add coordinates on a given diagonal path.
     * @param path A path object to be filled.
     * @param rowCoord  The initial row coordinate of the piece making the path.
     * @param colCoord  The initial column coordinate of the piece making the path.
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Nothing.
     */
    public void makeDiagonalPath(Path path, int rowCoord, int colCoord, int newRowCoord, int newColCoord) {
        if (newRowCoord - rowCoord != newColCoord - colCoord) {
            System.out.println("Invalid coordinates");
            return;
        }

        int horizontalCoord = rowCoord;
        int verticalCoord = colCoord;

        while(horizontalCoord != newRowCoord) {
            path.addCoords(horizontalCoord, verticalCoord);
            if (rowCoord < newRowCoord) {
                horizontalCoord++;
            }
            if (rowCoord > newRowCoord) {
                horizontalCoord--;
            }
            if (colCoord < newColCoord) {
                verticalCoord++;
            }
            if (colCoord > newColCoord) {
                verticalCoord--;
            }
        }
    }

    /**
     * Add coordinates on a given Vertical path.
     * @param path A path object to be filled.
     * @param rowCoord  The initial row coordinate of the piece making the path.
     * @param colCoord  The initial column coordinate of the piece making the path.
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Nothing.
     */
    public void makeVerticalPath(Path path, int rowCoord, int colCoord, int newRowCoord, int newColCoord) {
        if (newColCoord < colCoord) {
            for (int i = newColCoord; i < colCoord; i++) {
                path.addCoords(rowCoord, i);
            }
        } else {
            for (int i = colCoord; i < newColCoord; i++) {
                path.addCoords(rowCoord, i);
            }
        }
    }

    /**
     * Add coordinates on a given horizontal path.
     * @param path A path object to be filled.
     * @param rowCoord  The initial row coordinate of the piece making the path.
     * @param colCoord  The initial column coordinate of the piece making the path.
     * @param newRowCoord  The destination row coordinate for the piece making the path.
     * @param newColCoord  The destination column coordinate of the piece making the path.
     * @return Nothing.
     */
    public void makeHorizontalPath(Path path, int rowCoord, int colCoord, int newRowCoord, int newColCoord) {
        if (newColCoord < colCoord) {
            for (int i = newColCoord; i < colCoord; i++) {
                path.addCoords(i, colCoord);
            }
        } else {
            for (int i = colCoord; i < newColCoord; i++) {
                path.addCoords(i, colCoord);
            }
        }
    }

    public boolean hasValidMoves(Board board) {
        return false;
    }
}