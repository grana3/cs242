package tests;

import game.ChessBoard;
import game.LabelPanel;
import game.Turn;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChessBoardTest {

    @Test
    public void init() {
        Turn turn = Turn.WHITE;
        LabelPanel labels = new LabelPanel(turn);

        ChessBoard board = new ChessBoard(8, 8, turn, labels);
        board.init();

        assertEquals(turn, board.turn);
        assertEquals(labels, board.labels);
        assertNotNull(board.spots);
        assertNotNull(board.logicBoard);
        assertNotNull(board.buttonsPressed);
        assertNotNull(board.moves);
    }
}