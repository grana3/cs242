package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.King;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class KingTest {

    @Test
    void movesLegally() {
        Board board = new Board(8,8);
        King king = new King(0, 4, true, "black");
        int newXCoord = 1;
        int newYCoord = 4;
        int wrongXCoord = 4;
        int wrongYCoord = 4;

        Path path = king.makePath(board, newXCoord, newYCoord);
        Path wrongPath = king.makePath(board, wrongXCoord, wrongYCoord);

        assertTrue(path.isValid());
        assertFalse(wrongPath.isValid());
    }
}