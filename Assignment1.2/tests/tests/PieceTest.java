package tests;

import board.Board;
import org.junit.jupiter.api.Test;
import path.Path;
import piece.Pawn;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PieceTest {

    @Test
    void getRowCoord() {
        Pawn pawn = new Pawn(0, 0, true, "white");

        assertEquals(0, pawn.getRowCoord());
    }

    @Test
    void getColCoord() {
        Pawn pawn = new Pawn(0, 0, true, "white");

        assertEquals(0, pawn.getColCoord());
    }

    @Test
    void getColor() {
        Pawn pawn = new Pawn(0, 0, true, "white");

        assertEquals("white", pawn.getColor());
    }

    @Test
    void isInPlay() {
        Pawn pawn = new Pawn(0, 0, true, "white");

        assertTrue(pawn.isInPlay());
    }

    @Test
    void setRowCoord() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        pawn.setRowCoord(8);

        assertEquals(8, pawn.getRowCoord());
    }

    @Test
    void setColCoord() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        pawn.setColCoord(8);

        assertEquals(8, pawn.getColCoord());
    }

    @Test
    void deletePiece() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        pawn.deletePiece();

        assertFalse(pawn.isInPlay());
    }

    @Test
    void movesWithinBoard() {
        Board board = new Board(8, 8);
        Pawn pawn = new Pawn(0, 0, true, "white");

        assertTrue(pawn.movesWithinBoard(board, 0, 0));
        assertFalse(pawn.movesWithinBoard(board, 8, 8));
        assertFalse(pawn.movesWithinBoard(board, -1, -1));
    }

    @Test
    void makeDiagonalPath() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        Path path = new Path();
        int rowCoord = 0;
        int colCoord = 0;
        int newRowCoord = 5;
        int newColCoord = 5;
        pawn.makeDiagonalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);

        printCoords(path);
    }

    @Test
    void makeVerticalPath() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        Path path = new Path();
        int rowCoord = 0;
        int colCoord = 0;
        int newRowCoord = 0;
        int newColCoord = 5;
        pawn.makeVerticalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);

        printCoords(path);
    }

    @Test
    void makeHorizontalPath() {
        Pawn pawn = new Pawn(0, 0, true, "white");
        Path path = new Path();
        int rowCoord = 0;
        int colCoord = 0;
        int newRowCoord = 5;
        int newColCoord = 0;
        pawn.makeHorizontalPath(path, rowCoord, colCoord, newRowCoord, newColCoord);

        printCoords(path);
    }

    // Debugging method for printing a path's coordinates
    private void printCoords(Path path) {
        List<int[]> coordinates = path.getCoordinates();

        for (int i = 0; i < 5; i++) {
            System.out.println("(" + coordinates.get(i)[0] + "," + coordinates.get(i)[1] + ")");
        }
    }
}