/**
 * The graph class comprises an adjacency list (implemented via a map) of vertices.
 */
class Graph {
    constructor() {
        this.vertexList = new Map();
    }

    addVertex(vertex) {
        this.vertexList.set(vertex, []);
    }

    addEdge(initialVertex, endVertex) {
        this.vertexList.get(initialVertex).push(endVertex);
        this.vertexList.get(endVertex).push(initialVertex);
    }

    getVertices() {
        return this.vertexList;
    }

    printGraph() {
        const keys = this.vertexList.keys();

        for (let key of keys) {
            let values = this.vertexList.get(key);
            let vertices = "";

            for (let val of values) {
                vertices += "{Type: " + val.getType() + ", Name: " + val.getName() + "} ";
            }

            console.log(key.getName() + " -> " + vertices);
        }
    }
}

module.exports = Graph;

