const express = require('express');
const app = express();
const fs = require("fs");

app.get('/', function (req, res) {
    res.end("Welcome to the API");
});

app.get('/actors', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const actors = JSON.parse(data)[0];
        console.log(actors);
        res.status(200).end(JSON.stringify(actors));
    });
});

app.get('/actors/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const actors = JSON.parse(data)[0];
        let actor = actors[req.params.id.split("_")[0] + " " + req.params.id.split("_")[1]];
        console.log(actor);
        if (!actor) {
            res.status(404).end("Not found");
        } else {
            res.status(200).end(JSON.stringify(actor));
        }
    });
});

app.get('/movies', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const movies = JSON.parse(data)[1];
        console.log(movies);
        res.status(200).end(JSON.stringify(movies));
    });
});

app.get('/movies/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const movies = JSON.parse(data)[1];
        const movieNoSpaces = req.params.id.split("_");
        let query = "";
        for (let i = 0; i < movieNoSpaces.length; i++) {
            if (i === movieNoSpaces.length - 1) {
                query += movieNoSpaces[i];
            } else {
                query += movieNoSpaces[i] + " ";
            }
        }
        let movie = movies[query];
        console.log(movie);
        if (!movie) {
            res.status(404).end("Not found");
        } else {
            res.status(200).end(JSON.stringify(movie));
        }
    });
});

app.put('/actors/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const actors = JSON.parse(data)[0];
        let actor = actors[req.params.id.split("_")[0] + " " + req.params.id.split("_")[1]];
        actor[Object.keys(req.query)[0]] = req.query[Object.keys(req.query)[0]];
        console.log(req.body);
        if (!actor) {
            res.status(404).end("Not found");
        } else {
            res.status(200).end(JSON.stringify(actor));
        }
    });
});

app.put('/movies/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const movies = JSON.parse(data)[1];
        let movie = movies[req.params.id.split("_")[0] + " " + req.params.id.split("_")[1]];
        movie[Object.keys(req.query)[0]] = req.query[Object.keys(req.query)[0]];
        console.log(movie);
        if (!movie) {
            res.status(404).end("Not found");
        } else {
            res.status(200).end(JSON.stringify(movie));
        }
    });
});

app.post('/actors', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const actors = JSON.parse(data)[0];
        actors[Object.keys(req.query)[0]] = req.query[Object.keys(req.query)[0]];
        console.log(req.query[Object.keys(req.query)[0]]);
        res.status(201).end(JSON.stringify(req.query));
    });
});

app.post('/movies', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const movies = JSON.parse(data)[1];
        movies[Object.keys(req.query)[0]] = req.query[Object.keys(req.query)[0]];
        console.log(Object.keys(req.query)[0]);
        res.status(201).end(JSON.stringify(req.query));
    });
});

app.delete('/actors/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const actors = JSON.parse(data)[0];
        delete actors[req.params.id.split("_")[0] + " " + req.params.id.split("_")[1]];
        console.log(data);
        res.status(200).end(JSON.stringify(actors));
    });
});

app.delete('/movies/:id', function (req, res) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        const movies = JSON.parse(data)[1];
        delete movies[req.params.id.split("_")[0] + " " + req.params.id.split("_")[1]];
        console.log(data);
        res.status(200).end(JSON.stringify(movies));
    });
});

const server = app.listen(8080, function () {
    let port = server.address().port;
    console.log("Example app listening at http://localhost:" + port)
});