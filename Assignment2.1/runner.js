const Graph = require("./graph.js");
const Movie = require("./graphNode/movie.js");
const Actor = require("./graphNode/actor.js");

/**
 * Parses a JSON file and adds vertices to the given graph.
 * @param graph The graph to which vertices will be added.
 * @param path The path of the desired JSON file.
 */
function parseJson(graph, path) {
    const fs = require("fs");
    const contents = fs.readFileSync(path);
    const actors = JSON.parse(contents)[0];
    const movies = JSON.parse(contents)[1];

    for (let node in actors) {
        let actor = new Actor(actors[node].name, actors[node].age, actors[node].total_gross, actors[node].movies);
        graph.addVertex(actor);
    }

    for (let node in movies) {
        let movie = new Movie(movies[node].name, movies[node].year, movies[node].box_office, movies[node].actors);
        graph.addVertex(movie);
    }
}


/**
 * Finds a vertex in a list of vertices given a name.
 * @param name The name of the vertex to be found.
 * @param graph The graph to be searched.
 * @returns {*} The instance of the desired vertex.
 */
function findNode(name, vertices) {
    // Iterate through the vertices to find the one whose name matches
    for (let node of vertices) {
        if (node[0].getName() === name) {
            return node[0];
        }
    }

    return null;
}

/**
 * Draws edges between all connected vertices.
 * @param graph The graph to be edited.
 */
function addEdges(vertices) {
    //const vertices = graph.getVertices();
    const nodes = vertices.keys();

    for (let node of nodes) {
        // If the node is an actor and its adjacency list is empty, add to it
        if (node.nodeType === "Actor" && vertices.get(node).length === 0) {
            for (let idx in node.movies) {
                const movie = node.movies[idx];
                if (findNode(movie, vertices)) {
                    graph.addEdge(node, findNode(movie, vertices));
                }
            }
        }
    }
}

function getConnections(actor, vertices) {
    const movies = actor.getMovies();

    for (let movie in movies) {
        const found = findNode(movies[movie], vertices);
        if (!found || found.getType() === "Actor") {
            return;
        }
        if (found.getActors().length > 1) {
            actor.addConnections(found.getActors().length - 1);
        }
    }
}

/**
 * Determines the desired number of hub actors.
 * @param maxActors Number of hub actors.
 * @param graph Graph to be analyzed.
 * @returns {*} A list of hub actors.
 */
function getHubActors(graph) {
    const vertices = graph.getVertices();
    const nodes = vertices.keys();
    let hubActors = [];
    for (let node of nodes) {
        if (node.nodeType === "Actor") {
            getConnections(node, vertices);
            if (node.getConnections() > 0) {
                hubActors.push({name: node.getName(), connections: node.getConnections()});
            }
        }
    }

    return hubActors;
}

function getAgeGroups(graph) {
    const vertices = graph.getVertices();
    const nodes = vertices.keys();
    // idx 0 -> 1-29, idx 1 -> 30-49, idx 2 -> 50-69, idx 3 -> 70+
    let ageGroups = [0, 0, 0, 0];
    for (let node of nodes) {
        if (node.nodeType === "Actor") {
            let age = node.getAge();
            if (age < 30) {
                ageGroups[0] += node.getTotalGross();
            } else if (age >= 30 && age < 50) {
                ageGroups[1] += node.getTotalGross();
            } else if (age >= 50 && age < 70) {
                ageGroups[2] += node.getTotalGross();
            } else {
                ageGroups[3] += node.getTotalGross();
            }
        }
    }

    return ageGroups;
}

// Main runner
let graph = new Graph();
parseJson(graph, "data.json");
const vertices = graph.getVertices();
addEdges(vertices);
console.log(getHubActors(graph));

const ages = getAgeGroups(graph);
console.log("Ages 1-29: $" + ages[0]);
console.log("Ages 30-49: $" + ages[1]);
console.log("Ages 50-69: $" + ages[2]);
console.log("Ages 70+: $" + ages[3]);
console.log(ages.sort());
//graph.printGraph();