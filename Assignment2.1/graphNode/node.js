/**
 * The node class represents a vertex on the graph.
 */
class Node {
    constructor(nodeType, name) {
        this.nodeType = nodeType;
        this.name = name;
    }

    getType() {
        return this.nodeType;
    }

    getName() {
        return this.name;
    }
}

module.exports = Node;